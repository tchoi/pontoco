(function ($) {
    $(document).ready(function(){
        $( "#frmLogin" ).validate( {
            rules: {
                senha: {
                    required: true,
                    minlength: 5
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                senha: {
                    required: "A senha não pode estar em branco",
                    minlength: "A senha deve ter mais de 5 caracteres"
                },
                email: "Digite um e-mail válido"

            }
        } );
    });
})(jQuery);