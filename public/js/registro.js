var table = null;
(function ($) {

  function verRegistro(id){
    var url = "registros/ver/"+id
    $.ajax({
      dataType:"html",
      type: "GET",
      url: url,
      success: function(data){
        bootbox.dialog({
          title: 'Detalhes do registro',
          message: data,
          size:"large",
          buttons: {
            salvar: {
              label: "OK",
              className: 'btn-success',
              callback: function(){
                return true;
              }
            }
          }
        });
        //.find("div.modal-dialog").addClass("largeWidth");
      },
      error: function(data){

      }
    });
  }

  $(document).ready(function(){
    var $table = $('#registros');
    table = $table.DataTable({
      buttons: [ 'print', 'excel', {
        extend: 'pdf',
        orientation: 'landscape',
        pageSize: 'A4'
      }],
      sDom: '<"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      processing: true,
      serverSide: true,
      "bPaginate": true,
      language:
          {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "",
            searchPlaceholder: "Pesquisar",
            "oPaginate": {
              "sNext": "Próximo",
              "sPrevious": "Anterior",
              "sFirst": "Primeiro",
              "sLast": "Último"
            },
            "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
            }
          },
      ajax: {
        url:'registros/lista',
        data: function (d) {
          d.usuario_id = $('#usuario_id').val();
        }
      },

      columns: [
        {data: 'registro_id', name: 'registro_id'},
        {data: 'usuario_nome', name: 'usuario_nome'},
        {data: 'cliente_razao_social', name: 'cliente_razao_social'},
        {
          data: 'registro_dh',
          name: 'registro_dh',
          render: function (data,type,row){
            return dateDB2BRTime(data);
          }
        },
        {data: 'registro_tipo', name: 'registro_tipo'},
        {data: 'registro_motivo', name: 'registro_motivo'},
        {
          data: 'action',
          name: 'action',
          orderable: false,
          searchable: false,
          render: function (data, type, row) {
            var btns = new Array();
            btns.push('<a class="btn btn-info editar_registro" data-id="'+data+'" href="javascript:void(0);"><i class="fa fa-edit"></i></a>')
            btns.push('<a class="btn btn-info ver_registro" data-id="'+data+'" href="javascript:void(0);"><i class="fa fa-search"></i></a>')
            return btns.join(" ");
          }
        }
      ],
      "order": [[ 1, 'asc' ]],
      "fnDrawCallback": function (data, type, full, meta) {
        $('[data-toggle="tooltip1"]').tooltip({
          placement: 'top',
          title: 'heyo',
          container: 'body',
          html: true,
          template:'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
        });
      }
    });

    $('#datatable-tabletools_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');

    window.setTimeout(function(){
      $("#datatable-tabletools_filter").find("input[type=search]").attr('placeholder',"Pesquisar");
    },800);

    $("#usuario_id").change(function(){
      table.draw();
    });

    $(document).on( 'click', '.ver_registro',function(){
      verRegistro($(this).data('id'));
    });

  });
})(jQuery);