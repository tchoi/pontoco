var table = null;

function codeAddress(address) {
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == 'OK') {

            $("#empresa_latitude").val(results[0].geometry.location.toJSON().lat);
            $("#empresa_longitude").val(results[0].geometry.location.toJSON().lng);
        }else{
            $("#empresa_latitude").val(0);
            $("#empresa_longitude").val(0);
        }

    });
}
(function ($) {
    function salvaEmpresa(id){
        var url = "empresa/"+id
        var address = new Array();
        address.push($("#empresa_logradouro").val());
        address.push(", "+$("#empresa_logradouro").val());
        address.push("- " + $("#empresa_bairro").val());
        address.push(", " + $("#empresa_cidade").val());
        address.push("- " + $("#empresa_uf").val());
        address.push(", " + $("#empresa_cep").val());
        codeAddress(address.join(" "));

        //return true;
        type = "POST";

        var formdata = null;
        var contentType = "application/x-www-form-urlencoded";
        var processData = true;
        if ($("#frmempresa").attr('enctype') == 'multipart/form-data') {
            formdata = new FormData($("#frmempresa").get(0));//seleciona classe form-horizontal adicionada na tag form do html
            contentType = false;
            processData = false;
        } else {
            formdata = $("#frmempresa").serialize();
        }
        waitingDialog.show("Salvando as informações");

        $.ajax({
            dataType:"json",
            type: type,
            url: url,
            data: formdata, // serializes the form's elements.
            contentType: contentType,
            processData: processData,
            cache: false,
            success: function(data){
                //alert(data); // show response from the php script.
                waitingDialog.hide();
                window.setTimeout(function(){
                    newAlertTitle("Empresa salvo com sucesso","Sucesso",function(){

                        bootbox.hideAll();
                    });
                },500);
            },
            error: function(data){
                waitingDialog.hide();
                window.setTimeout(function(){
                    newAlertTitle("Erro ao tentar salvar a empresa","Erro",function(){

                        waitingDialog.hide()
                        bootbox.hideAll();
                    });
                },500);

            }
        });
    }
    $(document).ready(function(){
        function limpa_formulário_cep(){
            // Limpa valores do formulário de cep.
            $("#empresa_logradouro").val("");
            $("#empresa_bairro").val("");
            $("#empresa_cidade").val("");
            $("#empresa_uf").val("");
            $("#empresa_latitude").val("");
            $("#empresa_longitude").val("");
        }

        function aguardeCep(){
            $("#empresa_logradouro").val("aguarde...");
            $("#empresa_bairro").val("aguarde...");
            $("#empresa_cidade").val("aguarde...");
        }

        $(document).on('blur','#empresa_cep',function(e){
            var cep = $(this).cleanVal();
            limpa_formulário_cep();
            aguardeCep();
            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    $("#empresa_logradouro").val(dados.logradouro);
                    $("#empresa_bairro").val(dados.bairro);
                    $("#empresa_cidade").val(dados.localidade);
                    $("#empresa_uf").val(dados.uf);
                    var address = new Array();
                    address.push(dados.logradouro);
                    address.push("- " + dados.bairro);
                    address.push(", " + dados.localidade);
                    address.push("- " + dados.uf);
                    address.push(", " + cep);
                    codeAddress(address.join(" "));
                } //end if.
                else {
                    //CEP pesquisado não foi encontrado.
                    limpa_formulário_cep();
                    newAlert("CEP não encontrado.");
                }
            });
        });

        $(document).on('blur','#empresa_numero',function(e){
            var address = new Array();
            address.push($("#empresa_logradouro").val());
            address.push(", "+$(this).val());
            address.push("- " + $("#empresa_bairro").val());
            address.push(", " + $("#empresa_cidade").val());
            address.push("- " + $("#empresa_uf").val());
            address.push(", " + $("#empresa_cep").val());
            codeAddress(address.join(" "));
        });

        $(document).on('click', '#salvar',function(){
            salvaEmpresa(1);
        });

    });
})(jQuery);