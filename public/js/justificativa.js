var table = null;
(function ($) {
  $(document).ready(function(){
    var $table = $('#justificativas');
    table = $table.DataTable({
      buttons: [ 'print', 'excel', {
        extend: 'pdf',
        orientation: 'landscape',
        pageSize: 'A4'
      }],
      sDom: '<"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      processing: true,
      serverSide: true,
      "bPaginate": true,
      language:
          {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "",
            searchPlaceholder: "Pesquisar",
            "oPaginate": {
              "sNext": "Próximo",
              "sPrevious": "Anterior",
              "sFirst": "Primeiro",
              "sLast": "Último"
            },
            "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
            }
          },
      ajax: {
        url:'justificativas/lista',
        data: function (d) {
          d.usuario_id = $('#usuario_id').val();
        }
      },

      columns: [
        {data: 'justificativa_id', name: 'justificativa_id'},
        {
          data: 'justificativa_dt',
          name: 'justificativa_dt',
          render: function (data,type,row){
            return dateDB2BR(data);
          }
        },
        {data: 'usuario_nome', name: 'usuario_nome'},

        {data: 'justificativa_titulo', name: 'justificativa_titulo'},
        {
          data: 'action',
          name: 'action',
          orderable: false,
          searchable: false,
          render: function (data, type, row) {
            return '<a class="btn btn-info editar_registro" data-id="'+data+'" href="javascript:void(0);"><i class="fa fa-edit"></i></a>'
          }
        }
      ],
      "order": [[ 1, 'asc' ]],
      "fnDrawCallback": function (data, type, full, meta) {
        $('[data-toggle="tooltip1"]').tooltip({
          placement: 'top',
          title: 'heyo',
          container: 'body',
          html: true,
          template:'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
        });
      }
    });

    $('#datatable-tabletools_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');

    window.setTimeout(function(){
      $("#datatable-tabletools_filter").find("input[type=search]").attr('placeholder',"Pesquisar");
    },800);

    $("#usuario_id").change(function(){
      table.draw();
    });

  });
})(jQuery);