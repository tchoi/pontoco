var table = null;

function codeAddress(address) {
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == 'OK') {

            $("#usuario_residencia_latitude").val(results[0].geometry.location.toJSON().lat);
            $("#usuario_residencia_longitude").val(results[0].geometry.location.toJSON().lng);
        }else{
            $("#usuario_residencia_latitude").val(0);
            $("#usuario_residencia_longitude").val(0);
        }

    });
}
(function ($) {
    function getUsuario(id){
        var url = "funcionario/"+id
        $.ajax({
            dataType:"html",
            type: "GET",
            url: url,
            success: function(data){
                bootbox.dialog({
                    title: 'Funcionário',
                    message: data,
                    size:"large",
                    buttons: {
                        salvar: {
                            label: "<i class='fa fa-save'></i> Salvar",
                            className: 'btn-success',
                            callback: function(){
                                var address = new Array();
                                address.push($("#usuario_residencia_logradouro").val());
                                address.push(", "+$("#usuario_residencia_logradouro").val());
                                address.push("- " + $("#usuario_residencia_bairro").val());
                                address.push(", " + $("#usuario_residencia_cidade").val());
                                address.push("- " + $("#usuario_residencia_uf").val());
                                address.push(", " + $("#usuario_residencia_cep").val());
                                codeAddress(address.join(" "));

                                //return true;
                                type = "POST";

                                var formdata = null;
                                var contentType = "application/x-www-form-urlencoded";
                                var processData = true;
                                if ($("#frmUsuario").attr('enctype') == 'multipart/form-data') {
                                    formdata = new FormData($("#frmUsuario").get(0));//seleciona classe form-horizontal adicionada na tag form do html
                                    contentType = false;
                                    processData = false;
                                } else {
                                    formdata = $("#frmUsuario").serialize();
                                }
                                waitingDialog.show("Salvando as informações");

                                $.ajax({
                                    dataType:"json",
                                    type: type,
                                    url: url,
                                    data: formdata, // serializes the form's elements.
                                    contentType: contentType,
                                    processData: processData,
                                    cache: false,
                                    success: function(data){
                                        //alert(data); // show response from the php script.
                                        waitingDialog.hide();
                                        window.setTimeout(function(){
                                            newAlertTitle("Usuario salvo com sucesso","Sucesso",function(){
                                                table.draw();
                                                bootbox.hideAll();
                                            });
                                        },500);
                                    },
                                    error: function(data){
                                        waitingDialog.hide();
                                        window.setTimeout(function(){
                                            newAlertTitle("Erro ao tentar salvar o usuario","Erro",function(){
                                                table.draw();
                                                waitingDialog.hide();
                                                bootbox.hideAll();
                                            });
                                        },500);

                                    }
                                });

                            }
                        },
                        cancel: {
                            label: "<i class='fa fa-times'></i> Fechar",
                            className: 'btn-danger',
                            callback: function(){
                                return true;
                            }
                        }
                    }
                });
                //.find("div.modal-dialog").addClass("largeWidth");
            },
            error: function(data){

            }
        });
    }
    $(document).ready(function(){
        var $table = $('#usuarios');
        table = $table.DataTable({
            buttons: [ 'print', 'excel', {
                extend: 'pdf',
                orientation: 'landscape',
                pageSize: 'A4'
            }],
            sDom: '<"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            serverSide: true,
            "bPaginate": true,
            language:
                {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "",
                    searchPlaceholder: "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                },
            ajax: {
                url:'funcionarios/lista',
                data: function (d) {
                }
            },

            columns: [
                {data: 'usuario_id', name: 'usuario_id'},
                {data: 'usuario_nome', name: 'usuario_nome'},
                {data: 'usuario_cpf', name: 'usuario_cpf'},
                {data: 'usuario_email', name: 'usuario_email'},
                {data: 'usuario_nivel_label', name: 'usuario_nivel_label'},
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                    render: function (data, type, row) {
                        return '<a class="btn btn-info editar_usuario" data-id="'+data+'" href="javascript:void(0);"><i class="fa fa-edit"></i></a>'
                    }
                }
            ],
            "order": [[ 1, 'asc' ]],
            "fnDrawCallback": function (data, type, full, meta) {
                $('[data-toggle="tooltip1"]').tooltip({
                    placement: 'top',
                    title: 'heyo',
                    container: 'body',
                    html: true,
                    template:'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                });
            }
        });

        $('#datatable-tabletools_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');

        window.setTimeout(function(){
            $("#datatable-tabletools_filter").find("input[type=search]").attr('placeholder',"Pesquisar");
        },800);


        $("#adicionar_usuario").click(function(){
            getUsuario("novo");
        });


        $(document).on( 'click', '.editar_usuario',function(){
            getUsuario($(this).data('id'));
        });

        function limpa_formulário_cep(){
            // Limpa valores do formulário de cep.
            $("#usuario_residencia_logradouro").val("");
            $("#usuario_residencia_bairro").val("");
            $("#usuario_residencia_cidade").val("");
            $("#usuario_residencia_uf").val("");
            $("#usuario_residencia_latitude").val("");
            $("#usuario_residencia_longitude").val("");
        }

        function aguardeCep(){
            $("#usuario_residencia_logradouro").val("aguarde...");
            $("#usuario_residencia_bairro").val("aguarde...");
            $("#usuario_residencia_cidade").val("aguarde...");
        }

        $(document).on('blur','#usuario_residencia_cep',function(e){
            var cep = $(this).cleanVal();
            limpa_formulário_cep();
            aguardeCep();
            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    $("#usuario_residencia_logradouro").val(dados.logradouro);
                    $("#usuario_residencia_bairro").val(dados.bairro);
                    $("#usuario_residencia_cidade").val(dados.localidade);
                    $("#usuario_residencia_uf").val(dados.uf);
                    var address = new Array();
                    address.push(dados.logradouro);
                    address.push("- " + dados.bairro);
                    address.push(", " + dados.localidade);
                    address.push("- " + dados.uf);
                    address.push(", " + cep);
                    codeAddress(address.join(" "));
                } //end if.
                else {
                    //CEP pesquisado não foi encontrado.
                    limpa_formulário_cep();
                    newAlert("CEP não encontrado.");
                }
            });
        });

        $(document).on('blur','#usuario_residencia_numero',function(e){
            var address = new Array();
            address.push($("#usuario_residencia_logradouro").val());
            address.push(", "+$(this).val());
            address.push("- " + $("#usuario_residencia_bairro").val());
            address.push(", " + $("#usuario_residencia_cidade").val());
            address.push("- " + $("#usuario_residencia_uf").val());
            address.push(", " + $("#usuario_residencia_cep").val());
            codeAddress(address.join(" "));
        });

    });
})(jQuery);