$.validator.setDefaults({
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

try {
    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "date-uk-pre": function (a) {
            if (a == null || a == "") {
                return 0;
            }
            var ukDatea = a.split('/');
            var year = ukDatea[2];
            var hours = 0;
            if (ukDatea[2].length > 4) {
                var yes = year.split(" ");
                year = yes[0];
                hours = yes[1].replace(/\:/gi, "");
            }
            var concat = year + ukDatea[1] + ukDatea[0] + hours;
            var soma = concat * 1;
            return soma;
        },

        "date-uk-asc": function (a, b) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },

        "date-uk-desc": function (a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });
} catch (err) {

}


function verifyObrigatorio() {
    if (jQuery(".obrigatorio").length > 0) {
        jQuery(".obrigatorio").removeClass('error');
        var obj;
        var qtd = 0;
        jQuery(".obrigatorio").each(function () {
            obj = jQuery(this);
            if (jQuery.trim(obj.val()) == '') {
                try {
                    obj.parent().addClass("has-error");
                } catch (err) {

                }
                qtd++;
            }
        });
        return (qtd > 0) ? false : true;
    } else {
        return true;
    }
}
var newAlert = function (mensagem, tempo) {
    bootbox.dialog({
        message: mensagem,
        title: "A L E R T A",
        buttons: {
            success: {
                label: "OK",
                className: "btn-success",
                callback: function () {
                    //Example.show("great success");
                    window.setTimeout(function(){
                        $('body').removeClass('wysihtml5-supported').css('padding-right','');
                    },500);
                    return true;
                }
            }
        }
    });
}

var newAlertTitle = function (mensagem, title, url) {

    bootbox.dialog({
        message: mensagem,
        title: title,
        buttons: {
            success: {
                label: "OK",
                className: "btn-success",
                callback: function () {
                    //Example.show("great success");
                    if((typeof url !== 'undefined')){
                        if(typeof url !== 'function'){
                            window.location.href = url;
                        }else{
                            url();
                        }
                    }
                    bootbox.hideAll();
                    window.setTimeout(function(){
                        $('body').removeClass('wysihtml5-supported').css('padding-right','');
                    },500);
                }
            }
        }
    });
}

function seconds2time(seconds) {
    var hours = Math.floor(seconds / 3600);
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    var seconds = seconds - (hours * 3600) - (minutes * 60);
    var time = "";

    if (hours != 0) {
        time = hours + ":";
    }
    if (minutes != 0 || time !== "") {
        minutes = (minutes < 10) ? "0" + minutes : String(minutes);
        time += minutes + ":";
    }
    if (time === "") {
        time = seconds + "s";
    } else {
        time += (seconds < 10) ? "0" + seconds : String(seconds);
    }
    return time;
}

//
// Use internal $.serializeArray to get list of form elements which is
// consistent with $.serialize
//
// From version 2.0.0, $.serializeObject will stop converting [name] values
// to camelCase format. This is *consistent* with other serialize methods:
//
//   - $.serialize
//   - $.serializeArray
//
// If you require camel casing, you can either download version 1.0.4 or map
// them yourself.
//


function moedaParaNumero(valor){
    return isNaN(valor) == false ? parseFloat(valor) :   parseFloat(valor.replace("R$","").replace(".","").replace(",","."));
}
function numeroParaMoeda(numero) {
    var numero = numero.toFixed(2).split('.');
    numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
}
function dateDB2BR(data){
    var dt = moment(data, "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY");
    return dt;
}
function dateDB2BRTime(data){
  var dt = moment(data, "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY HH:mm:ss");
  return dt;
}
(function ($) {
    moment.locale('pt-br');
    bootbox.addLocale('pt-br', {
        OK: 'OK',
        CANCEL: 'Cancelar',
        CONFIRM: 'Confirmar'
    });
    bootbox.setDefaults({
        locale: "pt-br"
    });
    $.fn.serializeObject = function () {
        "use strict";

        var result = {};
        var extend = function (i, element) {
            var node = result[element.name];

            // If node with same name exists already, need to convert it to an array as it
            // is a multi-value field (i.e., checkboxes)

            if ('undefined' !== typeof node && node !== null) {
                if ($.isArray(node)) {
                    node.push(element.value);
                } else {
                    result[element.name] = [node, element.value];
                }
            } else {
                result[element.name] = element.value;
            }
        };

        $.each(this.serializeArray(), extend);
        return result;
    };
    $.fn.modal.Constructor.prototype.enforceFocus = function () {};
    $(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip({"container":"body"});
        $('.date').mask('00/00/0000');
        $('.time').mask('00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.phone').mask('0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.celphone_with_ddd').mask('(00) 00000-0000');
        $('.phone_us').mask('(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        //$('.cpf').mask('000.000.000-00', {reverse: true});
        //$('.cnpj').mask('00.000.000/0000-00', {reverse: true});

        $('.cpf').cpfcnpj({
            mask: true,
            validate: 'cpf',
            //event: 'focusout',
            // validateOnlyFocus: true,
            //handler: '.btn',
            ifValid: function (input) { input.removeClass("error"); },
            ifInvalid: function (input) { input.addClass("error"); }
        });

        $('.cnpj').cpfcnpj({
            mask: true,
            validate: 'cnpj',
            //event: 'focusout ',
            // validateOnlyFocus: true,
            //handler: '.btn',
            ifValid: function (input) { input.removeClass("error"); },
            ifInvalid: function (input) { input.addClass("error"); }
        });

        $('.cpfcnpj').cpfcnpj({
            mask: true,
            validate: 'cpfcnpj',
            //event: 'focusout ',
            // validateOnlyFocus: true,
            //handler: '.btn',
            ifValid: function (input) { input.removeClass("error"); },
            ifInvalid: function (input) { input.addClass("error"); }
        });


        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.money2').mask("#.##0,00", {reverse: true});
    });
})(jQuery);