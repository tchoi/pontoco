var table = null;
(function ($) {
    function getAgenda(id){
        var url = "agenda/"+id
        $.ajax({
            dataType:"html",
            type: "GET",
            url: url,
            success: function(data){
                bootbox.dialog({
                    title: 'Agenda',
                    message: data,
                    size:"large",
                    buttons: {
                        salvar: {
                            label: "<i class='fa fa-save'></i> Salvar",
                            className: 'btn-success',
                            callback: function(){
                                //return true;
                                type = "POST";

                                var formdata = null;
                                var contentType = "application/x-www-form-urlencoded";
                                var processData = true;
                                if ($("#frmAgenda").attr('enctype') == 'multipart/form-data') {
                                    formdata = new FormData($("#frmAgenda").get(0));//seleciona classe form-horizontal adicionada na tag form do html
                                    contentType = false;
                                    processData = false;
                                } else {
                                    formdata = $("#frmAgenda").serialize();
                                }
                                waitingDialog.show("Salvando as informações");

                                $.ajax({
                                    dataType:"json",
                                    type: type,
                                    url: url,
                                    data: formdata, // serializes the form's elements.
                                    contentType: contentType,
                                    processData: processData,
                                    cache: false,
                                    success: function(data){
                                        //alert(data); // show response from the php script.
                                        waitingDialog.hide();
                                        window.setTimeout(function(){
                                            newAlertTitle("Agenda salva com sucesso","Sucesso",function(){
                                                table.draw();
                                                bootbox.hideAll();
                                            });
                                        },500);
                                    },
                                    error: function(data){
                                        waitingDialog.hide();
                                        window.setTimeout(function(){
                                            newAlertTitle("Erro ao tentar salvar a agenda","Erro",function(){
                                                table.draw();
                                                waitingDialog.hide();
                                                bootbox.hideAll();
                                            });
                                        },500);

                                    }
                                });

                            }
                        },
                        cancel: {
                            label: "<i class='fa fa-times'></i> Fechar",
                            className: 'btn-danger',
                            callback: function(){
                                return true;
                            }
                        }
                    }
                });
                //.find("div.modal-dialog").addClass("largeWidth");
            },
            error: function(data){

            }
        });
    }
    $(document).ready(function(){
        var $table = $('#agendas');
        table = $table.DataTable({
            buttons: [ 'print', 'excel', {
                extend: 'pdf',
                orientation: 'landscape',
                pageSize: 'A4'
            }],
            sDom: '<"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p',
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            serverSide: true,
            "bPaginate": true,
            language:
                {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "",
                    searchPlaceholder: "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                },
            ajax: {
                url:'agendas/lista',
                data: function (d) {
                    d.razao_social = $('#frm-filter #razao_social').val();
                }
            },

            columns: [
                {data: 'agenda_id', name: 'agenda_id'},
                {data: 'usuario_nome', name: 'usuario_nome'},
                {data: 'cliente_razao_social', name: 'cliente_razao_social'},
                {data: 'agenda_dia_semana_label', name: 'agenda_dia_semana_label'},
                {
                    data: 'agenda_hora_entrada_01',
                    name: 'agenda_hora_entrada_01',
                    render: function (data, type, row) {
                        return row.agenda_hora_entrada_01+"-"+row.agenda_hora_saida_01+"/"+row.agenda_hora_entrada_02+"-"+row.agenda_hora_saida_02;
                        //return data;
                    }
                },
                {
                    data: 'agenda_dt_ini',
                    name: 'agenda_dt_ini',
                    render: function (data, type, row) {
                        return data!=null ? dateDB2BR(data) : "";
                        //return data;
                    }
                },
                {
                    data: 'agenda_dt_fim',
                    name: 'agenda_dt_fim',
                    render: function (data, type, row) {
                        return data!=null ? dateDB2BR(data) : "";
                    }
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                    render: function (data, type, row) {
                        return '<a class="btn btn-info editar_agenda" data-id="'+data+'" href="javascript:void(0);"><i class="fa fa-edit"></i></a>'
                    }
                }
            ],
            "order": [[ 1, 'asc' ]],
            "fnDrawCallback": function (data, type, full, meta) {
                $('[data-toggle="tooltip1"]').tooltip({
                    placement: 'top',
                    title: 'heyo',
                    container: 'body',
                    html: true,
                    template:'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                });
            }
        });

        $('#datatable-tabletools_wrapper').find('.btn-secondary').removeClass('btn-secondary').addClass('btn-default');

        window.setTimeout(function(){
            $("#datatable-tabletools_filter").find("input[type=search]").attr('placeholder',"Pesquisar");
        },800);


        $("#adicionar_agenda").click(function(){
            getAgenda("novo");
        });

        $(document).on( 'click', '.editar_agenda',function(){
            getAgenda($(this).data('id'));
        });
    });
})(jQuery);