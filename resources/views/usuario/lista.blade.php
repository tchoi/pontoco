<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 17/09/18
 * Time: 13:07
 */
?>
@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            Usuários
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url()}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Usuários</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Listagem</h3>
            </div>
            <div class="box-body">
                <div class="row m-b-10">
                    <div class="col-md-4">
                        <a href="javascript:void(0);" class="btn btn-success" id="adicionar_usuario"><i class="fa fa-plus"></i> Adicionar Usuário</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="usuarios" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>CPF</th>
                                <th>E-mail</th>
                                <th>Nível</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
@push('scripts')
    <script src="{{assets('js/usuario.js')}}"></script>
@endpush
