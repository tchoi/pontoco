<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 20/09/18
 * Time: 09:42
 */
$estados = getEstados();
?>
<form action="javascript:void(0)" id="frmUsuario" enctype="multipart/form-data">
    <input type="hidden" name="usuario_id" id="usuario_id" value="{{valueText($usuario->usuario_id)}}" />
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#perfil" aria-controls="perfil" role="tab" data-toggle="tab">Perfil</a></li>
        <li role="presentation"><a href="#endereco" aria-controls="endereco" role="tab" data-toggle="tab">Endereço</a></li>
        <li role="presentation"><a href="#agenda" aria-controls="agenda" role="tab" data-toggle="tab">Agenda</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active p-t-5" id="perfil">

            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="usuario_nome">Nome</label>
                    <input type="text" name="usuario_nome" id="usuario_nome" class="form-control"
                           value="{{valueText($usuario->usuario_nome)}}" placeholder="Nome" />
                </div>
                <div class="col-md-6 form-group">
                    <label for="usuario_dt_nascimento">Dt. Nascimento</label>
                    <input type="text" name="usuario_dt_nascimento" id="usuario_dt_nascimento" class="form-control"
                           value="{{\PontoCo\Helpers\Formatters::dateDB2BR(valueText($usuario->usuario_dt_nascimento))}}" placeholder="Dt. Nascimento" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="usuario_documento">CPF</label>
                    <input type="text" name="usuario_cpf" id="usuario_cpf" class="form-control cpfcnpj"
                           value="{{valueText($usuario->usuario_cpf)}}" placeholder="CPF" />
                </div>
                <div class="col-md-6 form-group">
                    <label for="usuario_rg">RG</label>
                    <input type="text" name="usuario_rg" id="usuario_rg" class="form-control"
                           value="{{valueText($usuario->usuario_rg)}}" placeholder="RG/Inscr. Estadual" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="usuario_responsavel_email">E-mail</label>
                    <input type="text" name="usuario_email" id="usuario_email" class="form-control"
                           value="{{valueText($usuario->usuario_email)}}" placeholder="E-mail" />
                </div>
                <div class="col-md-6 form-group">
                    <label for="usuario_senha">Senha</label>
                    <input type="password" name="usuario_senha" id="usuario_senha" class="form-control"
                           value="{{valueText($usuario->usuario_senha)}}" placeholder="Senha" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="nivel_usuario_id">Nível</label>
                    <select id="nivel_usuario_id" name="nivel_usuario_id" class="form-control" required>
                        <option value="">Selecione</option>
                        @foreach($nivelUsuario AS $k=>$v)
                            <option value="{{$v->nivel_usuario_id}}" {{selected($usuario->nivel_usuario_id,$v->nivel_usuario_id)}}>{{$v->nivel_usuario_titulo}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        </div>
        <div role="tabpanel" class="tab-pane p-t-5" id="endereco">
            <input type="hidden" class="form-control" id="usuario_residencia_latitude" name="usuario_residencia[usuario_residencia_latitude]"
                   value="{{valueText($usuario->usuario_residencia_latitude)}}" placeholder="CEP" />

            <input type="hidden" class="form-control" id="usuario_residencia_longitude" name="usuario_residencia[usuario_residencia_longitude]"
                   value="{{valueText($usuario->usuario_residencia_longitude)}}" placeholder="CEP">
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="usuario_residencia_cep">CEP</label>

                    <input type="text" class="form-control cep" id="usuario_residencia_cep" name="usuario_residencia[usuario_residencia_cep]"
                           value="{{valueText($usuario->usuario_residencia_cep)}}" placeholder="CEP">
                </div>
                <div class="form-group col-md-7">
                    <label for="usuario_residencia_logradouro">Logradouro</label>
                    <input type="text" class="form-control" id="usuario_residencia_logradouro" name="usuario_residencia[usuario_residencia_logradouro]"
                           value="{{valueText($usuario->usuario_residencia_logradouro)}}" placeholder="Logradouro">
                </div>
                <div class="form-group col-md-2">
                    <label for="usuario_residencia_numero">nº</label>
                    <input type="text" class="form-control" id="usuario_residencia_numero" name="usuario_residencia[usuario_residencia_numero]"
                           value="{{valueText($usuario->usuario_residencia_numero)}}" placeholder="Núm">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <label for="usuario_residencia_complemento">Complemento</label>
                    <input type="text" class="form-control" id="usuario_residencia_complemento" name="usuario_residencia[usuario_residencia_complemento]"
                           value="{{valueText($usuario->usuario_residencia_complemento)}}" placeholder="Complemento">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-4">
                    <label for="usuario_residencia_bairro">Bairro</label>
                    <input type="text" class="form-control" id="usuario_residencia_bairro" name="usuario_residencia[usuario_residencia_bairro]"
                           value="{{valueText($usuario->usuario_residencia_bairro)}}" placeholder="Bairro">
                </div>
                <div class="form-group col-md-4">
                    <label for="usuario_residencia_cidade">Cidade</label>
                    <input type="text" class="form-control" id="usuario_residencia_cidade" name="usuario_residencia[usuario_residencia_cidade]"
                           value="{{valueText($usuario->usuario_residencia_cidade)}}" placeholder="Cidade">
                </div>
                <div class="form-group col-md-4">
                    <label for="usuario_residencia_uf">Estado</label>
                    <select class="form-control" id="usuario_residencia_uf" name="usuario_residencia[usuario_residencia_uf]">
                        <option value="">Selecione</option>
                        @foreach($estados AS $k=>$v)
                            <option value="{{$k}}" {{selected($usuario->usuario_residencia_uf,$k)}}>{{$v}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        </div>
        <div role="tabpanel" class="tab-pane p-t-5" id="agenda">
            <div class="row">
                <div class="col-md-12 p-t-5">
                    <table id="agenda" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Cliente</th>
                            <th>Dias da Semana</th>
                            <th>Horários</th>
                            <th>Dt. Inicio</th>
                            <th>Dt. Fim</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $agenda as $row )
                            <tr>
                                <td>{{$row->agenda_id}}</td>
                                <td>{{$row->cliente_razao_social}}</td>
                                <td>{{$row->agenda_dia_semana}}</td>
                                <td>{{$row->agenda_hora_entrada_01}}-{{$row->agenda_hora_saida_01}}/{{$row->agenda_hora_entrada_02}}-{{$row->agenda_hora_saida_02}}</td>
                                <td>{{$row->agenda_dt_ini}}</td>
                                <td>{{$row->agenda_dt_fim}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>
<script src="{{assets('js/usuario_crud.js')}}"></script>

