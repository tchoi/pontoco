<?php
/**
 * Created by PhpStorm.
 * User: Tchoi
 * Date: 30/10/2018
 * Time: 22:09
 */
?>
<div class="row">
    <div class="col-md-12">
        <address>
            <strong>Dia</strong><br />
            {{\PontoCo\Helpers\Formatters::dateDB2BRTime($registro->registro_dh)}}
        </address>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <address>
            <strong>Funcionário</strong><br />
            {{$registro->usuario_nome}}
        </address>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <address>
            <strong>Cliente</strong><br />
            {{$registro->cliente_razao_social}}
        </address>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <address>
            <strong>Tipo</strong><br />
            {{$registro->registro_tipo}}
        </address>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <address>
            <strong>Motivo</strong><br />
            {{$registro->registro_motivo}}
        </address>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <address>
            <strong>Localização</strong><br />
            <div id="map" style="height:250px;width:100%"></div>
        </address>
    </div>
</div>

<script type="text/javascript">
  var map;
  $(document).ready(function(){
    map = new GMaps({
      div: '#map',
      lat: {{$registro->cliente_endereco_latitude}},
      lng: {{$registro->cliente_endereco_longitude}},
    });

    map.addMarker({
      lat: {{$registro->registro_latitude}},
      lng: {{$registro->registro_longitude}},
      title: '{{"Registro de ".$registro->registro_tipo}}',
      infoWindow: {
        content: '<p>{{"Registro de ".$registro->registro_tipo}}</p>'
      }
    });

    map.addMarker({
      lat: {{$registro->cliente_endereco_latitude}},
      lng: {{$registro->cliente_endereco_longitude}},
      title: '{{"Cliente: ".$registro->cliente_razao_social}}',
      infoWindow: {
        content: '<p>{{"Cliente: ".$registro->cliente_razao_social}}</p>'
      }
    });

    var latLng = [];
    latLng.push(new google.maps.LatLng({{$registro->registro_latitude}},{{$registro->registro_longitude}}));
    latLng.push(new google.maps.LatLng({{$registro->cliente_endereco_latitude}},{{$registro->cliente_endereco_longitude}}));

    map.fitLatLngBounds(latLng);
    window.setTimeout(function () {
      $("#map").css({'width':'100%',"height":"350px"});
    },500);

  });
</script>
