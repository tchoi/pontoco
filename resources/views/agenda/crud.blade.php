<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 21/10/18
 * Time: 13:51
 */
?>
<form action="javascript:void(0)" id="frmAgenda" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="usuario_id">Funcionário</label>
            <select class="form-control" id="usuario_id" name="usuario_id">
                <option value="">Selecione</option>
                @foreach($usuarios AS $k=>$v)
                    <option value="{{$v->usuario_id}}" {{selected($agenda->usuario_id,$v->usuario_id)}}>{{$v->usuario_nome}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 form-group">
            <label for="cliente_id">Cliente</label>
            <select class="form-control" id="cliente_id" name="cliente_id">
                <option value="">Selecione</option>
                @foreach($clientes AS $k=>$v)
                    <option value="{{$v->cliente_id}}" {{selected($agenda->cliente_id,$v->cliente_id)}}>{{$v->cliente_razao_social}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 form-group">
            <label for="agenda_dt_ini">Data Inicio</label>
            <input type="text" name="agenda_dt_ini" id="agenda_dt_ini" class="form-control date"
                   value="{{valueText($agenda->agenda_dt_ini) != "" ? date("d/m/Y",strtotime($agenda->agenda_dt_ini)):""}}" placeholder="Data Inicio" />
        </div>
        <div class="col-md-6 form-group">
            <label for="agenda_dt_fim">Data Termino</label>
            <input type="text" name="agenda_dt_fim" id="agenda_dt_fim" class="form-control date"
                   value="{{valueText($agenda->agenda_dt_fim) != "" ? date("d/m/Y",strtotime($agenda->agenda_dt_fim)):""}}" placeholder="Data Termino" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label for="agenda_dia_semana">Dia da Semana</label>
            <select class="form-control" id="agenda_dia_semana" name="agenda_dia_semana[]" multiple>
                <?php
                    $aDiaSemana = explode(",",$agenda->agenda_dia_semana);
                ?>
                @foreach(PontoCo\Helpers\Populators::getDiaSemana() AS $k=>$v)
                    <option value="{{$k}}" {{selectedInArray($aDiaSemana,$k)}}>{{$v}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 form-group">
            <label for="agenda_hora_entrada_01">1ª Entrada</label>
            <input type="text" name="agenda_hora_entrada_01" id="agenda_hora_entrada_01" class="form-control time"
                   value="{{valueText($agenda->agenda_hora_entrada_01)}}" placeholder="Entrada (Manhã)" />
        </div>
        <div class="col-md-3 form-group">
            <label for="agenda_hora_saida_01">1ª Saída</label>
            <input type="text" name="agenda_hora_saida_01" id="agenda_hora_saida_01" class="form-control time"
                   value="{{valueText($agenda->agenda_hora_saida_01)}}" placeholder="Saida (Manhã)" />
        </div>
        <div class="col-md-3 form-group">
            <label for="agenda_hora_entrada_02">2ª Entrada</label>
            <input type="text" name="agenda_hora_entrada_02" id="agenda_hora_entrada_02" class="form-control time"
                   value="{{valueText($agenda->agenda_hora_entrada_02)}}" placeholder="Entrada (Tarde)" />
        </div>
        <div class="col-md-3 form-group">
            <label for="agenda_hora_saida_02">2ª Saída</label>
            <input type="text" name="agenda_hora_saida_02" id="agenda_hora_saida_02" class="form-control time"
                   value="{{valueText($agenda->agenda_hora_saida_02)}}" placeholder="Saida (Tarde)" />
        </div>
    </div>
</form>
