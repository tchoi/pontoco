<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 17/09/18
 * Time: 12:47
 */
$base = new \PontoCo\Http\Controllers\BaseController();
$base->verifyLogin();

$session = $base->getSessions();
$iniciais = iniciais(titulo_site(),false);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{titulo_site()}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{url_public()}}bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{url_public()}}bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{url_public()}}bower_components/Ionicons/css/ionicons.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{url_public()}}bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">


    <!-- Theme style -->
    <link rel="stylesheet" href="{{url_public()}}dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{url_public()}}dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="{{assets("css/margins-paddings.css")}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-red sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="{{url()}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>{{$iniciais}}</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">{{$iniciais}}</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{url_public()}}dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{$session['usuario_nome']}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{url_public()}}dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                <p>
                                    {{$session['usuario_nome']}} - {{$session['usuario_nivel_label']}}
                                    <small>Member since Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Perfil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{url("logoff")}}" class="btn btn-default btn-flat">Sair</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{url_public()}}dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{$session['usuario_nome']}}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> {{$session['usuario_nivel_label']}}</a>
                </div>
            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Menu</li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Funcionários</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url("funcionarios")}}"><i class="fa fa-circle-o"></i> Listagem</a></li>
                        <li><a href="{{url("agendas")}}"><i class="fa fa-circle-o"></i> Agendas</a></li>
                        <li><a href="{{url("registros")}}"><i class="fa fa-circle-o"></i> Registros</a></li>
                        <li><a href="{{url("justificativas")}}"><i class="fa fa-circle-o"></i> Justificativas</a></li>
                        <li><a href="{{url("hora-extra")}}"><i class="fa fa-circle-o"></i> Horas Extras</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Clientes</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url("clientes")}}"><i class="fa fa-circle-o"></i> Listagens</a></li>
                    </ul>
                </li>

                <li><a href="{{url('empresa')}}"><i class="fa fa-circle-o"></i> Editar Empresa</a></li>
                <li><a href="{{url("logoff")}}"><i class="fa fa-power-off"></i> <span>Sair</span></a></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @yield('content')
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2018 <a href="http://vps4782.publiccloud.com.br">Walter Table</a>.</strong> All rights
        reserved.
    </footer>


</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{url_public()}}bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{url_public()}}bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="{{url_public()}}bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{url_public()}}bower_components/fastclick/lib/fastclick.js"></script>

<!-- DataTables -->
<script src="{{url_public()}}bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{url_public()}}bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<script src="{{assets("bower_components/bootbox.js/bootbox.js")}}"></script>
<script src="{{assets("bower_components/jquery-mask-plugin/dist/jquery.mask.js")}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="{{assets("bower_components/bootstrap-waitingfor/src/waitingfor.js")}}"></script>
<script src="{{assets("js/jquery.cpfcnpj.min.js")}}"></script>

<script src="{{assets("bower_components/moment/moment.js")}}"></script>



<!-- AdminLTE App -->
<script src="{{url_public()}}dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{url_public()}}dist/js/demo.js"></script>
<script>
    $(document).ready(function () {
        $('.sidebar-menu').tree()
    })
</script>
<script src="{{assets("js/utils.js")}}"></script>
<script src="{{assets("js/app.js")}}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0nNEAxQUOc4ARciftMxmUI05879w81cw&callback=initialize"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>

@stack('scripts')
</body>
</html>

