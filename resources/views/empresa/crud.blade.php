<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 17/09/18
 * Time: 13:07
 */
$estados = getEstados();
?>
@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            Empresa
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url()}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Empresa</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edição</h3>
            </div>
            <div class="box-body">
                <form id="frmempresa" enctype="multipart/form-data">
                    <input type="hidden" name="empresa_id" id="empresa_id" value="{{valueText($empresa->empresa_id)}}" />
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#perfil" aria-controls="perfil" role="tab" data-toggle="tab">Perfil</a></li>
                        <li role="presentation"><a href="#endereco" aria-controls="endereco" role="tab" data-toggle="tab">Endereço</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active p-t-5" id="perfil">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label for="empresa_tipo_pessoa">Tipo Pessoa</label>
                                    <select name="empresa_tipo_pessoa" id="empresa_tipo_pessoa" class="form-control" placeholder="Tipo Pessoa">
                                        <option value="PF" {{selected($empresa->empresa_tipo_pessoa,"PF")}}>Pessoa Física</option>
                                        <option value="PJ" {{selected($empresa->empresa_tipo_pessoa,"PJ")}}>Pessoa Jurídica</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="empresa_razao_social">Nome/Razão Social</label>
                                    <input type="text" name="empresa_razao_social" id="empresa_razao_social" class="form-control"
                                           value="{{valueText($empresa->empresa_razao_social)}}" placeholder="Nome/Razão Social" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="empresa_nome_fantasia">Nome Fantasia</label>
                                    <input type="text" name="empresa_nome_fantasia" id="empresa_nome_fantasia" class="form-control"
                                           value="{{valueText($empresa->empresa_nome_fantasia)}}" placeholder="Nome Fantasia" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="empresa_documento">CPF/CNPJ</label>
                                    <input type="text" name="empresa_documento" id="empresa_documento" class="form-control cpfcnpj"
                                           value="{{valueText($empresa->empresa_documento)}}" placeholder="CPF/CNPJ" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="empresa_ie">RG/Inscr. Estadual</label>
                                    <input type="text" name="empresa_ie" id="empresa_ie" class="form-control"
                                           value="{{valueText($empresa->empresa_ie)}}" placeholder="RG/Inscr. Estadual" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="empresa_responsavel">Responsável</label>
                                    <input type="text" name="empresa_responsavel" id="empresa_responsavel" class="form-control"
                                           value="{{valueText($empresa->empresa_responsavel)}}" placeholder="Responsável" />
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="empresa_responsavel_email">E-mail</label>
                                    <input type="text" name="empresa_responsavel_email" id="empresa_responsavel_email" class="form-control"
                                           value="{{valueText($empresa->empresa_responsavel_email)}}" placeholder="E-mail" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label for="empresa_logo">Logo</label>
                                    <input type="file" name="empresa_logo" id="empresa_logo" class="form-control" />
                                </div>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane p-t-5" id="endereco">
                            <input type="hidden" class="form-control" id="empresa_latitude" name="empresa[empresa_latitude]"
                                   value="{{valueText($empresa->empresa_latitude)}}" placeholder="CEP" />

                            <input type="hidden" class="form-control" id="empresa_longitude" name="empresa[empresa_longitude]"
                                   value="{{valueText($empresa->empresa_longitude)}}" placeholder="CEP">
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="empresa_cep">CEP</label>

                                    <input type="text" class="form-control cep" id="empresa_cep" name="empresa[empresa_cep]"
                                           value="{{valueText($empresa->empresa_cep)}}" placeholder="CEP">
                                </div>
                                <div class="form-group col-md-7">
                                    <label for="empresa_logradouro">Logradouro</label>
                                    <input type="text" class="form-control" id="empresa_logradouro" name="empresa[empresa_logradouro]"
                                           value="{{valueText($empresa->empresa_logradouro)}}" placeholder="Logradouro">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="empresa_numero">nº</label>
                                    <input type="text" class="form-control" id="empresa_numero" name="empresa[empresa_numero]"
                                           value="{{valueText($empresa->empresa_numero)}}" placeholder="Núm">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="empresa_complemento">Complemento</label>
                                    <input type="text" class="form-control" id="empresa_complemento" name="empresa[empresa_complemento]"
                                           value="{{valueText($empresa->empresa_complemento)}}" placeholder="Complemento">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="empresa_bairro">Bairro</label>
                                    <input type="text" class="form-control" id="empresa_bairro" name="empresa[empresa_bairro]"
                                           value="{{valueText($empresa->empresa_bairro)}}" placeholder="Bairro">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="empresa_cidade">Cidade</label>
                                    <input type="text" class="form-control" id="empresa_cidade" name="empresa[empresa_cidade]"
                                           value="{{valueText($empresa->empresa_cidade)}}" placeholder="Cidade">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="empresa_uf">Estado</label>
                                    <select class="form-control" id="empresa_uf" name="empresa[empresa_uf]">
                                        <option value="">Selecione</option>
                                        @foreach($estados AS $k=>$v)
                                            <option value="{{$k}}" {{selected($empresa->empresa_uf,$k)}}>{{$v}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void(0);" id="salvar" class="btn btn-primary pull-right">Salvar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
@push('scripts')
    <script src="{{assets('js/empresa.js')}}"></script>
@endpush
