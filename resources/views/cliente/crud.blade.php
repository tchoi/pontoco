<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 20/09/18
 * Time: 09:42
 */
$estados = getEstados();
?>
<form action="javascript:void(0)" id="frmCliente" enctype="multipart/form-data">
    <input type="hidden" name="cliente_id" id="cliente_id" value="{{valueText($cliente->cliente_id)}}" />
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#perfil" aria-controls="perfil" role="tab" data-toggle="tab">Perfil</a></li>
        <li role="presentation"><a href="#endereco" aria-controls="endereco" role="tab" data-toggle="tab">Endereço</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active p-t-5" id="perfil">
            <div class="row">
                <div class="col-md-12 form-group">
                    <label for="cliente_tipo_pessoa">Tipo Pessoa</label>
                    <select name="cliente_tipo_pessoa" id="cliente_tipo_pessoa" class="form-control" placeholder="Tipo Pessoa">
                        <option value="PF" {{selected($cliente->cliente_tipo_pessoa,"PF")}}>Pessoa Física</option>
                        <option value="PJ" {{selected($cliente->cliente_tipo_pessoa,"PJ")}}>Pessoa Jurídica</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="cliente_razao_social">Nome/Razão Social</label>
                    <input type="text" name="cliente_razao_social" id="cliente_razao_social" class="form-control"
                           value="{{valueText($cliente->cliente_razao_social)}}" placeholder="Nome/Razão Social" />
                </div>
                <div class="col-md-6 form-group">
                    <label for="cliente_nome_fantasia">Nome Fantasia</label>
                    <input type="text" name="cliente_nome_fantasia" id="cliente_nome_fantasia" class="form-control"
                           value="{{valueText($cliente->cliente_nome_fantasia)}}" placeholder="Nome Fantasia" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="cliente_documento">CPF/CNPJ</label>
                    <input type="text" name="cliente_documento" id="cliente_documento" class="form-control cpfcnpj"
                           value="{{valueText($cliente->cliente_documento)}}" placeholder="CPF/CNPJ" />
                </div>
                <div class="col-md-6 form-group">
                    <label for="cliente_ie">RG/Inscr. Estadual</label>
                    <input type="text" name="cliente_ie" id="cliente_ie" class="form-control"
                           value="{{valueText($cliente->cliente_ie)}}" placeholder="RG/Inscr. Estadual" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="cliente_responsavel">Responsável</label>
                    <input type="text" name="cliente_responsavel" id="cliente_responsavel" class="form-control"
                           value="{{valueText($cliente->cliente_responsavel)}}" placeholder="Responsável" />
                </div>
                <div class="col-md-6 form-group">
                    <label for="cliente_responsavel_email">E-mail</label>
                    <input type="text" name="cliente_responsavel_email" id="cliente_responsavel_email" class="form-control"
                           value="{{valueText($cliente->cliente_responsavel_email)}}" placeholder="E-mail" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 form-group">
                    <label for="cliente_logo">Logo</label>
                    <input type="file" name="cliente_logo" id="cliente_logo" class="form-control" />
                </div>
            </div>

        </div>
        <div role="tabpanel" class="tab-pane p-t-5" id="endereco">
            <input type="hidden" class="form-control" id="cliente_endereco_latitude" name="cliente_endereco[cliente_endereco_latitude]"
                   value="{{valueText($cliente->cliente_endereco_latitude)}}" placeholder="CEP" />

            <input type="hidden" class="form-control" id="cliente_endereco_longitude" name="cliente_endereco[cliente_endereco_longitude]"
                   value="{{valueText($cliente->cliente_endereco_longitude)}}" placeholder="CEP">
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="cliente_endereco_cep">CEP</label>

                    <input type="text" class="form-control cep" id="cliente_endereco_cep" name="cliente_endereco[cliente_endereco_cep]"
                           value="{{valueText($cliente->cliente_endereco_cep)}}" placeholder="CEP">
                </div>
                <div class="form-group col-md-7">
                    <label for="cliente_endereco_logradouro">Logradouro</label>
                    <input type="text" class="form-control" id="cliente_endereco_logradouro" name="cliente_endereco[cliente_endereco_logradouro]"
                           value="{{valueText($cliente->cliente_endereco_logradouro)}}" placeholder="Logradouro">
                </div>
                <div class="form-group col-md-2">
                    <label for="cliente_endereco_numero">nº</label>
                    <input type="text" class="form-control" id="cliente_endereco_numero" name="cliente_endereco[cliente_endereco_numero]"
                           value="{{valueText($cliente->cliente_endereco_numero)}}" placeholder="Núm">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <label for="cliente_endereco_complemento">Complemento</label>
                    <input type="text" class="form-control" id="cliente_endereco_complemento" name="cliente_endereco[cliente_endereco_complemento]"
                           value="{{valueText($cliente->cliente_endereco_complemento)}}" placeholder="Complemento">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-4">
                    <label for="cliente_endereco_bairro">Bairro</label>
                    <input type="text" class="form-control" id="cliente_endereco_bairro" name="cliente_endereco[cliente_endereco_bairro]"
                           value="{{valueText($cliente->cliente_endereco_bairro)}}" placeholder="Bairro">
                </div>
                <div class="form-group col-md-4">
                    <label for="cliente_endereco_cidade">Cidade</label>
                    <input type="text" class="form-control" id="cliente_endereco_cidade" name="cliente_endereco[cliente_endereco_cidade]"
                           value="{{valueText($cliente->cliente_endereco_cidade)}}" placeholder="Cidade">
                </div>
                <div class="form-group col-md-4">
                    <label for="cliente_endereco_uf">Estado</label>
                    <select class="form-control" id="cliente_endereco_uf" name="cliente_endereco[cliente_endereco_uf]">
                        <option value="">Selecione</option>
                        @foreach($estados AS $k=>$v)
                            <option value="{{$k}}" {{selected($cliente->cliente_endereco_uf,$k)}}>{{$v}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        </div>
    </div>
</form>