@extends('layouts.app')
@section('content')
<section class="content-header">
    <h1>
        Bem Vindo
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url()}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Home</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Bem Vindo</h3>
        </div>
        <div class="box-body">
            Seja Bem Vindo
        </div>
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
@endsection
@push('scripts')
    <script src="{{assets('js/home.js')}}"></script>
@endpush