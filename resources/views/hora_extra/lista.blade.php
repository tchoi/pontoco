<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 12/11/18
 * Time: 12:30
 */
?>
@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            Horas Extras
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url()}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Horas Extras</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Listagem</h3>
            </div>
            <div class="box-body">
                <div class="row m-b-10">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="usuario_id">Usuário</label>
                            <select id="usuario_id" name="usuario_id" class="form-control">
                                <option value="">Todos</option>
                                @foreach( $usuarios AS $usuario )
                                    <option value="{{$usuario->usuario_id}}">{{$usuario->usuario_nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="horas_extras" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Data</th>
                                <th>Funcionário</th>
                                <th>Motivo</th>
                                <th>Início</th>
                                <th>Termino</th>
                                <th>Status</th>
                                <th>Ação</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
@push('scripts')
    <script src="{{assets('js/hora_extra.js')}}"></script>
@endpush
