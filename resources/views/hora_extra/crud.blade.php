<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 12/11/18
 * Time: 12:30
 */
?>
<form action="javascript:void(0)" id="frmHoraExtra" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-6 form-group">
            <label for="hora_extra_dh_view">Data da Solicitação</label>
            <input type="text" name="hora_extra_dh_view" id="hora_extra_dh_view" class="form-control"
                   value="{{date("d/m/Y H:i:s",strtotime($hora_extra->hora_extra_dh))}}" placeholder="Data da Solicitação" readonly />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="funcionario_solicitante">Funcionário Solicitante</label>
            <input type="text" name="funcionario_solicitante" id="funcionario_solicitante" class="form-control"
                   value="{{valueText($hora_extra->usuario_nome)}}" placeholder="Funcionário Solicitante" readonly />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="hora_extra_motivo_view">Motivo</label>
            <textarea readonly name="hora_extra_motivo_view" id="hora_extra_motivo_view" class="form-control">{{valueText($hora_extra->hora_extra_motivo)}}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 form-group">
            <label for="hora_extra_periodo_ini">Início</label>
            <input type="text" name="hora_extra_periodo_ini" id="hora_extra_periodo_ini" class="form-control date_time"
                   value="{{valueText($hora_extra->hora_extra_periodo_ini) != "" ? date("d/m/Y H:i:s",strtotime($hora_extra->hora_extra_periodo_ini)):""}}" placeholder="Início" />
        </div>
        <div class="col-md-6 form-group">
            <label for="hora_extra_periodo_fim">Termino</label>
            <input type="text" name="hora_extra_periodo_fim" id="hora_extra_periodo_fim" class="form-control date_time"
                   value="{{valueText($hora_extra->hora_extra_periodo_fim) != "" ? date("d/m/Y H:i:s",strtotime($hora_extra->hora_extra_periodo_fim)):""}}" placeholder="Termino" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="hora_extra_motivo_status">Justificativa</label>
            <textarea name="hora_extra_motivo_status" id="hora_extra_motivo_status" class="form-control">{{valueText($hora_extra->hora_extra_motivo_status)}}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 form-group">
            <label for="hora_extra_status">Status</label>
            <select name="hora_extra_status" id="hora_extra_status" class="form-control">
                <option value="P" {{selected("P",$hora_extra->hora_extra_status)}}>Pendente</option>
                <option value="A" {{selected("A",$hora_extra->hora_extra_status)}}>Aprovado</option>
                <option value="R" {{selected("R",$hora_extra->hora_extra_status)}}>Reprovado</option>
            </select>
        </div>
    </div>
</form>
