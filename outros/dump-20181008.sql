CREATE DATABASE  IF NOT EXISTS `walter_dot` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `walter_dot`;
-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: walter_dot
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda` (
  `agenda_id` bigint(19) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_id` bigint(19) unsigned NOT NULL,
  `cliente_id` bigint(19) unsigned NOT NULL,
  `agenda_hora_entrada_01` time DEFAULT NULL,
  `agenda_hora_entrada_02` time DEFAULT NULL,
  `agenda_hora_saida_01` time DEFAULT NULL,
  `agenda_hora_saida_02` time DEFAULT NULL,
  `agenda_dt_ini` date DEFAULT NULL,
  `agenda_dt_fim` date DEFAULT NULL,
  PRIMARY KEY (`agenda_id`),
  KEY `fk_agenda_usuario1_idx` (`usuario_id`),
  KEY `fk_agenda_cliente1_idx` (`cliente_id`),
  CONSTRAINT `fk_agenda_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`cliente_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_agenda_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agenda`
--

LOCK TABLES `agenda` WRITE;
/*!40000 ALTER TABLE `agenda` DISABLE KEYS */;
/*!40000 ALTER TABLE `agenda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `cliente_id` bigint(19) unsigned NOT NULL AUTO_INCREMENT,
  `empresa_id` bigint(19) unsigned NOT NULL,
  `cliente_tipo_pessoa` enum('PF','PJ') NOT NULL,
  `cliente_razao_social` varchar(255) NOT NULL,
  `cliente_nome_fantasia` varchar(255) NOT NULL,
  `cliente_documento` varchar(45) NOT NULL,
  `cliente_ie` varchar(45) DEFAULT NULL,
  `cliente_endereco_cep` varchar(9) NOT NULL,
  `cliente_endereco_logradouro` varchar(255) NOT NULL,
  `cliente_endereco_numero` varchar(10) NOT NULL,
  `cliente_endereco_complemento` text,
  `cliente_endereco_bairro` varchar(255) NOT NULL,
  `cliente_endereco_cidade` varchar(255) NOT NULL,
  `cliente_endereco_uf` varchar(2) NOT NULL,
  `cliente_endereco_latitude` decimal(25,6) NOT NULL,
  `cliente_endereco_longitude` decimal(25,6) NOT NULL,
  `cliente_responsavel` varchar(255) DEFAULT NULL,
  `cliente_responsavel_email` varchar(255) DEFAULT NULL,
  `cliente_status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`cliente_id`),
  KEY `fk_cliente_empresa1_idx` (`empresa_id`),
  CONSTRAINT `fk_cliente_empresa1` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`empresa_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,1,'PJ','FAM','FAM','21.212.212/1212-12','212122221','01304-001','Rua Augusta','1508','','ConsolaÃ§Ã£o','SÃ£o Paulo','SP',-23.556594,-46.658823,'212121221','testes@testes.com.br','A');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `empresa_id` bigint(19) unsigned NOT NULL AUTO_INCREMENT,
  `empresa_tipo_pessoa` enum('PF','PJ') NOT NULL DEFAULT 'PJ',
  `empresa_razao_social` varchar(255) NOT NULL,
  `empresa_nome_fantasia` varchar(255) NOT NULL,
  `empresa_documento` varchar(45) NOT NULL,
  `empresa_cep` varchar(9) NOT NULL,
  `empresa_logradouro` varchar(255) NOT NULL,
  `empresa_numero` varchar(10) NOT NULL,
  `empresa_complemento` text,
  `empresa_bairro` varchar(255) DEFAULT NULL,
  `empresa_cidade` varchar(255) DEFAULT NULL,
  `empresa_uf` varchar(2) DEFAULT NULL,
  `empresa_latitude` decimal(25,6) DEFAULT NULL,
  `empresa_longitude` decimal(25,6) DEFAULT NULL,
  PRIMARY KEY (`empresa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'PJ','Testes','Testes','241541541','','','','','','','',0.000000,0.000000);
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nivel_usuario`
--

DROP TABLE IF EXISTS `nivel_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nivel_usuario` (
  `nivel_usuario_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nivel_usuario_titulo` varchar(45) DEFAULT NULL,
  `nivel_usuario_descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`nivel_usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nivel_usuario`
--

LOCK TABLES `nivel_usuario` WRITE;
/*!40000 ALTER TABLE `nivel_usuario` DISABLE KEYS */;
INSERT INTO `nivel_usuario` VALUES (1,'Administrador','Administrador'),(2,'RH','RH'),(3,'Funcionario','Funcionario');
/*!40000 ALTER TABLE `nivel_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro`
--

DROP TABLE IF EXISTS `registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro` (
  `registro_id` bigint(19) unsigned NOT NULL,
  `usuario_id` bigint(19) unsigned NOT NULL,
  `cliente_id` bigint(19) unsigned NOT NULL,
  `registro_dh` datetime NOT NULL,
  `registro_latitude` decimal(25,8) DEFAULT NULL,
  `registro_longitude` decimal(25,8) DEFAULT NULL,
  PRIMARY KEY (`registro_id`),
  KEY `fk_registro_usuario1_idx` (`usuario_id`),
  KEY `fk_registro_cliente1_idx` (`cliente_id`),
  CONSTRAINT `fk_registro_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`cliente_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro`
--

LOCK TABLES `registro` WRITE;
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;
/*!40000 ALTER TABLE `registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `usuario_id` bigint(19) unsigned NOT NULL AUTO_INCREMENT,
  `empresa_id` bigint(19) unsigned NOT NULL,
  `nivel_usuario_id` int(10) unsigned NOT NULL,
  `usuario_nome` varchar(255) NOT NULL,
  `usuario_email` varchar(255) NOT NULL,
  `usuario_senha` varchar(45) NOT NULL,
  `usuario_dt_nascimento` date DEFAULT NULL,
  `usuario_residencia_logradouro` varchar(255) DEFAULT NULL,
  `usuario_residencia_numero` varchar(10) DEFAULT NULL,
  `usuario_residencia_bairro` varchar(255) DEFAULT NULL,
  `usuario_residencia_cidade` varchar(255) DEFAULT NULL,
  `usuario_residencia_uf` varchar(2) DEFAULT NULL,
  `usuario_residencia_cep` varchar(9) DEFAULT NULL,
  `usuario_residencia_latitude` decimal(25,8) DEFAULT NULL,
  `usuario_residencia_longitude` decimal(25,8) DEFAULT NULL,
  PRIMARY KEY (`usuario_id`),
  KEY `fk_usuario_nivel_usuario_idx` (`nivel_usuario_id`),
  KEY `fk_usuario_empresa1_idx` (`empresa_id`),
  CONSTRAINT `fk_usuario_empresa1` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`empresa_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_nivel_usuario` FOREIGN KEY (`nivel_usuario_id`) REFERENCES `nivel_usuario` (`nivel_usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,1,1,'Jonas Ferreira','inboxfox@gmail.com','destino2002','1984-02-20','Rua das Hortensias','343','Vila Ipê','Jandira','SP','06606330',0.00000000,0.00000000);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'walter_dot'
--

--
-- Dumping routines for database 'walter_dot'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-08 10:08:48
