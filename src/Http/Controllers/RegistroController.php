<?php
/**
 * Created by PhpStorm.
 * User: Tchoi
 * Date: 23/10/2018
 * Time: 21:13
 */

namespace PontoCo\Http\Controllers;


use Jacwright\RestServer\RestException;
use PontoCo\Helpers\Datatable;
use PontoCo\Helpers\Formatters;
use PontoCo\Helpers\Sanetizers;
use PontoCo\Models\Agenda;
use PontoCo\Models\Cliente;
use PontoCo\Models\Justificativa;
use PontoCo\Models\Registro;
use PontoCo\Models\Usuario;

class RegistroController extends BaseController
{

    /**
     * Mostra a tela
     *
     * @url GET /registros
     */
    public function index(){
        $usuarios = Usuario::getList([])->get();

        $this->returnView('registro.lista', ['name' => 'hello world','usuarios'=>$usuarios]);
    }

    /**
     * Listagem de agendas
     *
     * @url GET /registros/lista
     * @url POST /registros/lista
     */
    public function lista(){
        $data = $this->getAll();
        $lista = Registro::getList($data);

        if(isset($data['mobile'])){
            return $this->returnToMobile($lista);
        }else{
            return Datatable::make($data,$lista);
        }
    }
    /**
     * Edição/Criação de Registro
     *
     * @url POST /registro/$id
     * @url POST /registro/novo
     */
    public function edit($id=null){
        $data = $this->getAll();

        if(isset($data['mobile'])){
            //Verifica se a pessoa não esta realizando o ponto da sua residencia
            $usuario = Usuario::getOne($data['usuario_id']);


            $distance = $this->codexworldGetDistanceOpt(
                $usuario->usuario_residencia_latitude,
                $usuario->usuario_residencia_longitude,
                $data['registro_latitude'],
                $data['registro_longitude']
            );



            $distance = $distance * 1000;
            if($distance <= 500){
                throw new RestException(403, 'Você não pode realizar o registro de sua residencia');
                die();
            }

            $data['registro_dh'] = Formatters::dateBR2DBTime($data['registro_dh']);
            $cliente = Agenda::getClienteByLatLng($data);
            $data['cliente_id'] = valueText($cliente->cliente_id,"");

            if(empty($data['cliente_id'])){
                throw new RestException(403, 'Você não possui uma agenda disponível para esse cliente');
                die();
            }
        }

        $data['registro_dh_ini'] = date("Y-m-d 00:00:00",strtotime($data['registro_dh']));
        $data['registro_dh_fim'] = date("Y-m-d 23:59:59",strtotime($data['registro_dh']));
        $registroAnterior = Registro::verifyUltimoRegistro($data);
        if( isset($registroAnterior->registro_id) ){
            if($data['registro_tipo']!='confirmacao' && $registroAnterior->registro_tipo==$data['registro_tipo']){
                throw new RestException(403, "Você não pode registrar uma {$data['registro_tipo']} 2 vezes seguidas");
                die();
            }
        }

        if(
            ($data['registro_tipo']=='pausa' && !isset($registroAnterior->registro_id))
            || ($data['registro_tipo']=='pausa' && $registroAnterior->registro_tipo=='saida_01')
            || ($data['registro_tipo']=='pausa' && $registroAnterior->registro_tipo=='saida_02') ){
            throw new RestException(403, 'Você não pode pedir uma pausa fora do expediente');
            die();
        }


        if (isset($id) && !empty($id) && $id!='novo') {
            $registro = Registro::edit($id,$data);
        }else{
            $registro = Registro::store($data);
        }
        if(!isset($registro->registro_id)){
            throw new RestException(403, 'Erro ao tentar salvar o registro');
            die();
        }
        return array("msg"=>"Registro salvo com sucesso","success"=>true,"registro"=>$registro);
    }
    /**
     * Verifica se existe a necessidade de Registro
     *
     * @url POST /registros/verifica-local
     */
    public function verificaLocal(){
        $distanciaMinima = 1000;
        $data = $this->getAll();

        $usuario = Usuario::getOne($data['usuario_id']);
        $usuario->clientes = Agenda::getClientes(['usuario_id'=>$data['usuario_id']]);

        $dh = date("Y-m-d H:i:s");
        $agenda = null;
        $agendas = Agenda::getClienteByHorarioLista([
            'usuario_id'=>$data['usuario_id'],
            'dh'=>$dh,
            'agenda_dia_semana'=>date("w")
        ]);
        $year = date("Y");
        foreach($agendas AS $agendaTmp){
            $cep = Formatters::limpaCep($agendaTmp->cliente_endereco_cep);
            $viaCep = json_decode(file_get_contents("http://viacep.com.br/ws/{$cep}/json/"));
            if(isset($viaCep->ibge)){
                $feriados = file_get_contents("https://api.calendario.com.br/?ano={$year}&ibge={$viaCep->ibge}&token=aW5ib3hmb3hAZ21haWwuY29tJmhhc2g9ODMwMjU0Ng&json=true");
                $feriado = false;
                foreach($feriados AS $fer){
                    if(date("d/m/Y")==$fer->date){
                        $feriado = true;
                        break;
                    }
                }

                if($feriado){
                    if(stripos($agendaTmp->agenda_dia_semana,"7")!==false){
                        $agenda = $agendaTmp;
                        break;
                    }
                }else{
                    if(stripos($agendaTmp->agenda_dia_semana,date("w"))!==false){
                        $agenda = $agendaTmp;
                        break;
                    }
                }
            }

        }

        if(!isset($agenda->cliente_id)){
            return array("usuario"=>$usuario,"msg"=>"Você não possui nenhuma agenda neste horário","success"=>false,"tipo"=>"");
        }

        $agenda_hora_entrada_01 = !empty($agenda->agenda_hora_entrada_01) ? date("U",strtotime(date("Y-m-d")." ".$agenda->agenda_hora_entrada_01)) : 0;
        $agenda_hora_saida_01 = !empty($agenda->agenda_hora_saida_01) ? date("U",strtotime(date("Y-m-d")." ".$agenda->agenda_hora_saida_01)) : 0;
        $agenda_hora_entrada_02 = !empty($agenda->agenda_hora_entrada_02) ? date("U",strtotime(date("Y-m-d")." ".$agenda->agenda_hora_entrada_02)) : 0;
        $agenda_hora_saida_02 = !empty($agenda->agenda_hora_saida_02) ? date("U",strtotime(date("Y-m-d")." ".$agenda->agenda_hora_saida_02)) : 0;
        $timestamp = date("U");

        $distance = $this->codexworldGetDistanceOpt(
            $agenda->cliente_endereco_latitude,
            $agenda->cliente_endereco_longitude,
            $data['registro_latitude'],
            $data['registro_longitude']
        );
        $distance = $distance * 1000;

        $ultimoRegistro = Registro::verifyUltimoRegistro([
            'usuario_id'=>$data['usuario_id'],
            'cliente_id'=>$agenda->cliente_id,
            'registro_dh_ini'=>date("Y-m-d 00:00:00"),
            'registro_dh_fim'=>date("Y-m-d 23:59:59")
        ]);


        //Determinar se é o primeiro registro do dia
        if( !isset($ultimoRegistro->registro_id) ){
            $dtJustificativa = date("Y-m-d H:i:s");
            $justificativa = Justificativa::whereRaw("('{$dtJustificativa}' BETWEEN justificativa_duracao_ini AND justificativa_duracao_fim)")
                ->where('usuario_id',$data['usuario_id'])
                ->first();

            if( isset($justificativa->justificativa_id) ){
                return array("usuario"=>$usuario,"msg"=>"Horário Justificado","success"=>false,"tipo"=>"");
            }

            //Se sim validar distancia
            if($distance >= $distanciaMinima){
                return array("usuario"=>$usuario,"msg"=>"Você deveria estar no cliente {$agenda->cliente_razao_social}","success"=>true,"tipo"=>"justificativa");
            }else{
                if($agenda_hora_entrada_01 > 0){
                    return array("usuario"=>$usuario,"msg"=>"Você está dentro do periodo da entrada no cliente {$agenda->cliente_razao_social}","success"=>true,"tipo"=>"entrada_01");
                }
            }
        }

        if($ultimoRegistro->registro_tipo=='entrada_01'){
            if($agenda_hora_saida_01 > 0 && ($agenda_hora_saida_01 - $timestamp) <= 600){
                return array("usuario"=>$usuario,"msg" => "Você está dentro do periodo da primeira saída do cliente {$agenda->cliente_razao_social}", "success" => true,"tipo"=>"saida_01");
            }else{
                return $this->confirmacaoIdentidade($usuario);
            }
        }
        if($ultimoRegistro->registro_tipo=='saida_01'){
            if($agenda_hora_entrada_02 > 0 && ($agenda_hora_entrada_02-$timestamp) <= 600){
                return array("usuario"=>$usuario,"msg" => "Você está dentro do periodo de retorno ao cliente {$agenda->cliente_razao_social}", "success" => true,"tipo"=>"entrada_02");
            }else{
                return $this->confirmacaoIdentidade($usuario);
            }
        }

        if($ultimoRegistro->registro_tipo=='entrada_02'){
            if($agenda_hora_saida_02 > 0 && ($agenda_hora_saida_02 - $timestamp) <= 600){
                return array("usuario"=>$usuario,"msg" => "Você está dentro do periodo de saida do cliente {$agenda->cliente_razao_social}", "success" => true,"tipo"=>"saida_02");
            }else{
                return $this->confirmacaoIdentidade($usuario);
            }
        }


    }


    protected function confirmacaoIdentidade($usuario){
        $num = mt_rand(0,1000);
        if($num % 3 < 1){
            return array("usuario"=>$usuario,"msg" => "Realize uma confirmação de identidade", "success" => true,"tipo"=>"confirmacao");
        }
        return array("usuario"=>$usuario,"msg"=>"Não esta no horário","success"=>false,"tipo"=>"");
    }
    public function verificaLocalOld(){
        $data = $this->getAll();
        $agenda= Agenda::getClienteByLatLng($data);
        $diaSemana = explode(",",$agenda->agenda_dia_semana);
        if(!isset($agenda->cliente_id)){
            return array("msg"=>"Não esta em um cliente","success"=>false);
        }



        if( in_array(date("w"),$diaSemana)===false ){
            return array("msg"=>"O dia não condiz","success"=>false);
        }

        $agenda_hora_entrada_01 = !empty($agenda->agenda_hora_entrada_01) ? date("U",strtotime(date("Y-m-d")." ".$agenda->agenda_hora_entrada_01)) : 0;
        $agenda_hora_saida_01 = !empty($agenda->agenda_hora_saida_01) ? date("U",strtotime(date("Y-m-d")." ".$agenda->agenda_hora_saida_01)) : 0;
        $agenda_hora_entrada_02 = !empty($agenda->agenda_hora_entrada_02) ? date("U",strtotime(date("Y-m-d")." ".$agenda->agenda_hora_entrada_02)) : 0;
        $agenda_hora_saida_02 = !empty($agenda->agenda_hora_saida_02) ? date("U",strtotime(date("Y-m-d")." ".$agenda->agenda_hora_saida_02)) : 0;

        $timestamp = date("U");

        $ultimoRegistro = Registro::verifyUltimoRegistro([
            'usuario_id'=>$data['usuario_id'],
            'cliente_id'=>$agenda->cliente_id,
            'registro_dh'=>date("Y-m-d H:i:s")
        ],'<');

        $diffUltimoRegistro = date("U",strtotime($ultimoRegistro->registro_dh));

        if($agenda_hora_entrada_01 > 0 && abs( $agenda_hora_entrada_01 - $timestamp  ) <= 600){
            if(abs( $diffUltimoRegistro - $timestamp ) >=600){
                return array("msg"=>"Você está dentro do periodo da entrada no cliente {$agenda->cliente_razao_social}","success"=>true,"tipo"=>"entrada");
            }
        }else if($agenda_hora_entrada_02 > 0 && abs( $agenda_hora_entrada_02 - $timestamp  ) <= 600){
            if(abs( $diffUltimoRegistro - $timestamp ) >=600) {
                return array("msg" => "Você está dentro do periodo de retorno ao cliente {$agenda->cliente_razao_social}", "success" => true,"tipo"=>"entrada");
            }
        }else if($agenda_hora_saida_01 > 0 && abs( $agenda_hora_saida_01 - $timestamp  ) <= 600){
            if(abs( $diffUltimoRegistro - $timestamp ) >=600) {
                return array("msg" => "Você está dentro do periodo da primeira saída do cliente {$agenda->cliente_razao_social}", "success" => true,"tipo"=>"saida");
            }
        }else if($agenda_hora_saida_02 > 0 && abs( $agenda_hora_saida_02 - $timestamp  ) <= 600){
            if(abs( $diffUltimoRegistro - $timestamp ) >=600) {
                return array("msg" => "Você está dentro do periodo de saida do cliente {$agenda->cliente_razao_social}", "success" => true,"tipo"=>"saida");
            }
        }else{
            return array("msg"=>"Não esta no horário","success"=>false,"tipo"=>"");
        }

    }

    /**
     * Formulario de Edição/Criação de Registros
     *
     * @url GET /registros/ver/$id
     */
    public function getOne($id=null){
        $data = $this->getAll();
        $registro = Registro::getRegistro($id);
        $this->returnView('registro.ver', ['registro' => $registro]);
    }
}