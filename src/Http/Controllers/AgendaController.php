<?php
/**
 * Created by PhpStorm.
 * User: Tchoi
 * Date: 21/10/2018
 * Time: 17:00
 */

namespace PontoCo\Http\Controllers;

use PontoCo\Helpers\Datatable;
use PontoCo\Helpers\Formatters;
use PontoCo\Models\Agenda;
use PontoCo\Models\Cliente;
use PontoCo\Models\Usuario;

class AgendaController extends BaseController
{

    /**
     * Mostra a tela
     *
     * @url GET /agendas
     */
    public function index(){
        $this->returnView('agenda.lista', ['name' => 'hello world']);
    }

    /**
     * Listagem de agendas
     *
     * @url GET /agendas/lista
     */
    public function lista(){
        $data = $this->getAll();
        $lista = Agenda::getList($data);

        return Datatable::make($data,$lista);

    }

    /**
     * Formulario de Edição/Criação de Agenda
     *
     * @url GET /agenda/$id
     * @url GET /agenda/novo
     */
    public function crud($id=null){
        $data = $this->getAll();
        $session = $this->getSessions();

        $agenda = null;
        $usuarios = null;
        if($session['nivel_usuario_id']=='1'){
            $usuarios = Usuario::getList()->get();
        }else{
            $usuarios = Usuario::where('usuario_id',$session['usuario_id'])->get();
        }

        $clientes = Cliente::getList()->get();
        if (isset($id) && !empty($id) && $id!='novo') {
            $agenda = Agenda::getOne($id);
        }
        $this->returnView('agenda.crud', [
                'agenda' => $agenda,
                'clientes'=>$clientes,
                'usuarios'=>$usuarios
        ]);
    }

    /**
     * Edição/Criação de Agendas
     *
     * @url POST /agenda/$id
     * @url POST /agenda/novo
     */
    public function edit($id=null){
        $data = $this->getAll();
        $data['agenda_dia_semana'] = implode(",",$data['agenda_dia_semana']);
        $data['agenda_dt_ini'] = isset($data['agenda_dt_ini']) && !empty($data['agenda_dt_ini']) ? Formatters::dateBR2DB($data['agenda_dt_ini']) : null;
        $data['agenda_dt_fim'] = isset($data['agenda_dt_fim']) && !empty($data['agenda_dt_fim']) ? Formatters::dateBR2DB($data['agenda_dt_fim']) : null;

        if (isset($id) && !empty($id) && $id!='novo') {
            $agenda = Agenda::edit($id,$data);
        }else{
            //$session = $this->getSessions();
            //$data['usuario_id'] = $session['usuario_id'];
            $agenda = Agenda::store($data);
        }


        if(!isset($agenda->agenda_id)){
            throw new RestException(404, 'Erro ao tentar salvar a agenda');
            die();
        }

        return array("success"=>"true","agenda"=>$agenda);
    }
}