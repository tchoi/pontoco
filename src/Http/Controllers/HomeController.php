<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 14/09/18
 * Time: 13:56
 */

namespace PontoCo\Http\Controllers;


class HomeController extends BaseController
{
    /**
     * Returns a JSON string object to the browser when hitting the root of the domain
     *
     * @url GET /
     */
    public function index(){
        echo $this->view->render('home', ['name' => 'hello world']);
    }
}