<?php
/**
 * Created by PhpStorm.
 * User: Tchoi
 * Date: 28/10/2018
 * Time: 21:54
 */

namespace PontoCo\Http\Controllers;


use Jacwright\RestServer\RestException;
use PontoCo\Helpers\Datatable;
use PontoCo\Helpers\Formatters;
use PontoCo\Models\Justificativa;
use PontoCo\Models\Usuario;

class JustificativaController extends BaseController
{
    /**
     * Mostra a tela
     *
     * @url GET /justificativas
     */
    public function index(){
        $usuarios = Usuario::getList([])->get();
        $this->returnView('justificativa.lista', ['name' => 'hello world','usuarios'=>$usuarios]);
    }

    /**
     * Listagem de justificativas
     *
     * @url GET /justificativas/lista
     * @url POST /justificativas/lista
     */
    public function lista(){
        $data = $this->getAll();
        $lista = Justificativa::getList($data);

        if(isset($data['mobile'])){
            return $this->returnToMobile($lista);
        }else{
            return Datatable::make($data,$lista);
        }
    }

    /**
     * Edição/Criação de Registro
     *
     * @url POST /justificativa/$id
     * @url POST /justificativa/novo
     */
    public function edit($id=null){
        $data = $this->getAll();
        $files = $this->getFiles();
        $aFiles = [];
        for($i=0;$i<count($files['arquivos']['name']);$i++){
            $file = date("YmdHis_").$files['arquivos']['name'][$i];
            $realFile = $this->anexoPath.$file;
            if( move_uploaded_file($files['arquivos']['tmp_name'][$i],$realFile) ){
                $aFiles[] = $file;
            }
        }
        $data['justificativa_arquivo'] = json_encode($aFiles);
        $data['justificativa_dt'] = date("Y-m-d");

        $data['justificativa_duracao_ini'] =  isset($data['justificativa_duracao_ini']) ? Formatters::dateBR2DB($data['justificativa_duracao_ini']) : null;
        $data['justificativa_duracao_fim'] =  isset($data['justificativa_duracao_fim']) ? Formatters::dateBR2DB($data['justificativa_duracao_fim']) : null;

        if (isset($id) && !empty($id) && $id!='novo') {
            $registro = Justificativa::edit($id,$data);
        }else{
            $registro = Justificativa::store($data);
        }
        if(!isset($registro->justificativa_id)){
            throw new RestException(403, 'Erro ao tentar salvar a justificativa');
            die();
        }
        return array("msg"=>"Justificativa salva com sucesso","success"=>true,"justificativa"=>$registro);
    }
}