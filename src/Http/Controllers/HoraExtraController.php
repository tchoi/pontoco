<?php
/**
 * Created by PhpStorm.
 * User: Tchoi
 * Date: 11/11/2018
 * Time: 19:48
 */

namespace PontoCo\Http\Controllers;


use PontoCo\Helpers\Datatable;
use PontoCo\Helpers\Formatters;
use PontoCo\Models\HoraExtra;
use PontoCo\Models\Usuario;

use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Device;
use paragraph1\phpFCM\Notification;

class HoraExtraController extends BaseController
{
    /**
     * Mostra a tela
     *
     * @url GET /hora-extra
     */
    public function index(){
        $usuarios = Usuario::getList([])->get();
        $this->returnView('hora_extra.lista', ['name' => 'hello world','usuarios'=>$usuarios]);
    }

    /**
     * Listagem de Horas Extras
     *
     * @url GET /hora-extra/lista
     * @url POST /hora-extra/lista
     */
    public function lista(){
        $data = $this->getAll();
        $lista = HoraExtra::getList($data);
        if(isset($data['mobile'])){
            return $this->returnToMobile($lista);
        }else{
            return Datatable::make($data,$lista);
        }
    }

    /**
     * Edição/Criação de Hora-Extra
     *
     * @url POST /hora-extra/$id
     * @url POST /hora-extra/novo
     */
    public function edit($id=null){
        $data = $this->getAll();
        $data['hora_extra_dh'] = date("Y-m-d H:i:s");
        $data['hora_extra_periodo_ini'] =  isset($data['hora_extra_periodo_ini']) ? Formatters::dateBR2DB($data['hora_extra_periodo_ini']) : null;
        $data['hora_extra_periodo_fim'] =  isset($data['hora_extra_periodo_fim']) ? Formatters::dateBR2DB($data['hora_extra_periodo_fim']) : null;

        if (isset($id) && !empty($id) && $id!='novo') {
            if($data['hora_extra_status']!='P'){
                $session = $this->getSessions();
                $data['usuario_id_aprovador'] = $session['usuario_id'];
            }
            $horaextra = HoraExtra::edit($id,$data);

        }else{
            $horaextra = HoraExtra::store($data);
        }
        
        if(!isset($horaextra->usuario_id)){
            throw new RestException(404, 'Erro ao tentar salvar a hora extra');
            die();
        }


        if($id!=null && $id!='novo'){
            $apiKey = 'AIzaSyCP7BioMqtLxy67nm_x1luQyE6ehbzk21g';
            $client = new Client();
            $client->setApiKey($apiKey);
            $client->injectHttpClient(new \GuzzleHttp\Client());

            $user = Usuario::getOne($horaextra->usuario_id);

            if(!empty($user->dispositivo_token)){
                $status = $horaextra->hora_extra_status == 'A' ? "Aprovado" : "Reprovado";
                $message = new Message();
                $message->addRecipient(new Device($user->dispositivo_token));
                $message->setData(array(
                    'title' => "Sua solicitação de hora extra foi {$status}",
                    'body'=>$horaextra->hora_extra_motivo_status,
                    'tipo'=>"NOTIFICACAO"
                ));

                $response = $client->send($message);
            }
        }else{
            $apiKey = 'AIzaSyCP7BioMqtLxy67nm_x1luQyE6ehbzk21g';
            $client = new Client();
            $client->setApiKey($apiKey);
            $client->injectHttpClient(new \GuzzleHttp\Client());

            $usuarios = Usuario::whereIn("usuario.nivel_usuario_id",['1','2'])->get();
            foreach($usuarios AS $user){
                if(empty($user->dispositivo_token)){
                    continue;
                }
                $message = new Message();
                $message->addRecipient(new Device($user->dispositivo_token));
                $message->setData(array(
                    'title' => "Hora Extra para aprovar",
                    'body'=>"Você tem solicitações de hora extra para aprovar",
                    'tipo'=>"AVISO_HORA_EXTRA"
                ));

                $response = $client->send($message);
            }
        }

        return array("success"=>"true","horaextra"=>$horaextra,"msg"=>"Hora extra salva com sucesso");

    }

    /**
     * Formulario de Edição de Solicitações de Hora Extra
     *
     * @url GET /hora-extra/$id
     * @url GET /hora-extra/novo
     */
    public function crud($id=null){
        $data = $this->getAll();
        $hora_extra = null;
        if (isset($id) && !empty($id) && $id!='novo') {
            $hora_extra = HoraExtra::getOne($id);
        }
        $this->returnView('hora_extra.crud', [
            'hora_extra' => $hora_extra
        ]);
    }

}