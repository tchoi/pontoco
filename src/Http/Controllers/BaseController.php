<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 14/09/18
 * Time: 10:18
 */

namespace PontoCo\Http\Controllers;


use DateInterval;
use DatePeriod;
use DateTime;
use Jenssegers\Blade\Blade;
use PontoCo\Models\Usuario;

class BaseController
{
    /**
     * BaseController constructor.
     */
    protected $view = null;
    protected $anexoPath = "";
    protected $imgUserPath = "";
    public function __construct()
    {
        $path_blade = dirname(__FILE__);
        $DS = DIRECTORY_SEPARATOR;
        $path_blade = "{$path_blade}{$DS}..{$DS}..{$DS}..{$DS}";
        $path_blade = realpath($path_blade) . $DS;
        $this->view = new Blade("{$path_blade}resources{$DS}views", "{$path_blade}resources{$DS}cache");


        $this->anexoPath = $path_blade."anexos".$DS;
        if( !is_dir($this->anexoPath) ){
            mkdir($this->anexoPath,0777,true);
        }
        chmod($this->anexoPath,0777);

        $this->imgUserPath = $path_blade."img_user".$DS;
        if( !is_dir($this->imgUserPath) ){
            mkdir($this->imgUserPath,0777,true);
        }
        chmod($this->imgUserPath,0777);

        $citiesTempoMaximo = 604800;
        $citiesFile = "{$path_blade}cities.json";
        if(!is_file($citiesFile)){
            file_put_contents($citiesFile,file_get_contents("http://www.calendario.com.br/api/cities.json"));
        }else{
            $diff = date("U") - date ("U", filemtime($citiesFile));
            if($diff > $citiesTempoMaximo){
                file_put_contents($citiesFile,file_get_contents("http://www.calendario.com.br/api/cities.json"));
            }
        }

    }

    protected function returnView($view,$params){
        header("Content-Type: text/html; charset=UTF-8",true);
        echo $this->view->render($view, $params);
    }

    protected function getPost(){
        $this->updatedUser($_POST);
        return $_POST;
    }
    protected function getQueryString(){
        $this->updatedUser($_GET);
        return $_GET;
    }
    protected function getAll(){
        $this->updatedUser($_REQUEST);
        return $_REQUEST;
    }
    protected function getFiles(){
        return $_FILES;
    }

    protected function updatedUser($data){
        if( isset($data['usuario_id']) ){
            $user = Usuario::getOne($data['usuario_id']);
            if( isset($user->usuario_id) && isset($data['mobile'])){
                if(isset($data['dispositivo_identificador']))
                    $user->dispositivo_identificador = $data['dispositivo_identificador'];

                if(isset($data['dispositivo_app_version']))
                    $user->dispositivo_app_version = $data['dispositivo_app_version'];

                if(isset($data['dispositivo_token']))
                    $user->dispositivo_token = $data['dispositivo_token'];
                $user->save();
            }
        }
    }

    protected function getDatesFromRange($start, $end, $format = 'Y-m-d') {
        $array = array();
        try {
            $interval = new DateInterval('P1D');
        } catch (\Exception $e) {
            return null;
        }

        $realEnd = new DateTime($end);
        $realEnd->add($interval);

        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

        foreach($period as $date) {
            $array[] = $date->format($format);
        }

        return $array;
    }

    public function registerSession($arr) {
        if (is_array($arr) && count($arr) > 0) {
            foreach ($arr AS $k => $v) {
                $_SESSION[\PontoCo\Helpers\Sanetizers::normaliza(app_name())][$k] = $v;
            }
        }
    }

    public function unRegisterSession($arr) {
        if (is_array($arr) && count($arr) > 0) {
            foreach ($arr AS $k => $v) {
                unset($_SESSION[\PontoCo\Helpers\Sanetizers::normaliza(app_name())][$k]);
            }
        }
    }
    public function unRegisterSessionAll() {
        unset($_SESSION[\PontoCo\Helpers\Sanetizers::normaliza(app_name())]);
    }

    public function getSessions() {
        return $_SESSION[\PontoCo\Helpers\Sanetizers::normaliza(app_name())];
    }

    public function verifyLogon() {
        $session = $this->getSessions();
        if (isset($session['usuario_email']) && trim($session['usuario_email']) != '') {
            header('Location: '.url("/"));
        }
    }
    public function verifyLogin() {
        $actual_link = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $session = $this->getSessions();
        if (!isset($session['usuario_email']) && trim($session['usuario_email']) == '') {
            $_SESSION['RETURN_URL'] = $actual_link;
            header('Location: '.url("login"));
            return false;
        }
        return true;
    }

    /**
     * Optimized algorithm from http://www.codexworld.com
     *
     * @param float $latitudeFrom
     * @param float $longitudeFrom
     * @param float $latitudeTo
     * @param float $longitudeTo
     *
     * @return float [km]
     */
    public function codexworldGetDistanceOpt($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
        $rad = M_PI / 180;
        //Calculate distance from latitude and longitude
        $theta = $longitudeFrom - $longitudeTo;
        $dist = sin($latitudeFrom * $rad)
                * sin($latitudeTo * $rad) +  cos($latitudeFrom * $rad)
                * cos($latitudeTo * $rad) * cos($theta * $rad);

        return acos($dist) / $rad * 60 *  1.853;
    }

    public function returnToMobile($lista){
        $regs = array();
        if($lista->count() > 0){
            $regs = $lista->get();
        }
        return ['data'=>$regs,"recordsFiltered"=>$lista->count(),"recordsTotal"=>$lista->count()];
    }
}