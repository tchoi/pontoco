<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 17/09/18
 * Time: 13:10
 */

namespace PontoCo\Http\Controllers;


use PontoCo\Helpers\Datatable;
use PontoCo\Models\Cliente;
use Jacwright\RestServer\RestException;

class ClienteController extends BaseController
{
    /**
     * Mostra a tela de login
     *
     * @url GET /clientes
     */
    public function index(){
        $this->returnView('cliente.lista', ['name' => 'hello world']);
    }

    /**
     * Listagem de clientes
     *
     * @url GET /clientes/lista
     */
    public function lista(){
        $data = $this->getAll();
        $lista = Cliente::getList($data);

        return Datatable::make($data,$lista);

    }

    /**
     * Formulario de Edição/Criação de Clientes
     *
     * @url GET /cliente/$id
     * @url GET /cliente/novo
     */
    public function crud($id=null){
        $data = $this->getAll();
        $cliente = null;
        if (isset($id) && !empty($id) && $id!='novo') {
            $cliente = Cliente::getOne($id);
        }
        $this->returnView('cliente.crud', ['cliente' => $cliente]);
    }

    /**
     * Edição/Criação de Clientes
     *
     * @url POST /cliente/$id
     * @url POST /cliente/novo
     */
    public function edit($id=null){
        $data = $this->getAll();

        foreach($data['cliente_endereco'] AS $k=>$v){
            $data[$k] = $v;
        }

        if (isset($id) && !empty($id) && $id!='novo') {
            $cliente = Cliente::edit($id,$data);
        }else{
            $session = $this->getSessions();
            $data['empresa_id'] = $session['empresa_id'];
            $cliente = Cliente::store($data);
        }


        if(!isset($cliente->cliente_id)){
            throw new RestException(404, 'Erro ao tentar salvar o cliente');
            die();
        }

        return array("success"=>"true","cliente"=>$cliente);
    }
}