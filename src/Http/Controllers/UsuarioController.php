<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 08/10/18
 * Time: 19:10
 */

namespace PontoCo\Http\Controllers;

use PontoCo\Helpers\Datatable;
use Jacwright\RestServer\RestException;
use PontoCo\Helpers\Formatters;
use PontoCo\Models\Agenda;
use PontoCo\Models\Usuario;
use PontoCo\Models\UsuarioNivel;

class UsuarioController extends BaseController
{
    /**
     * Mostra a tela de listagem de funcionarios
     *
     * @url GET /funcionarios
     */
    public function index(){
        $this->returnView('usuario.lista', ['name' => 'hello world']);
    }

    /**
     * Listagem de funcionarios
     *
     * @url GET /funcionarios/lista
     */
    public function lista(){
        $data = $this->getAll();
        $lista = Usuario::getList($data);
        $ret = Datatable::make($data,$lista);
        return $ret;

    }

    /**
     * Formulario de Edição/Criação de Funcionarios
     *
     * @url GET /funcionario/$id
     * @url GET /funcionario/novo
     */
    public function crud($id=null){
        $data = $this->getAll();
        $usuario = null;
        $agendas = null;
        if (isset($id) && !empty($id) && $id!='novo') {
            $usuario = Usuario::getOne($id);
            $agendas = Agenda::getList([
                    'usuario_id'=>$id
            ])->get();
        }
        $nivelUsuario = UsuarioNivel::all();
        $this->returnView('usuario.crud', [
                'usuario' => $usuario,
                'nivelUsuario'=>$nivelUsuario,
                'agenda'=>$agendas
        ]);
    }

    /**
     * Edição/Criação de Funcionarios
     *
     * @url POST /funcionario/$id
     * @url POST /funcionario/novo
     */
    public function edit($id=null){
        $data = $this->getAll();
        $files = $this->getFiles();
        $data['usuario_dt_nascimento'] = isset($data['usuario_dt_nascimento']) ? Formatters::dateBR2DB($data['usuario_dt_nascimento']) : null;

        if(isset($data['usuario_residencia'])){
            foreach($data['usuario_residencia'] AS $k=>$v){
                $data[$k] = $v;
            }
        }


        if( isset($files['usuario_imagem']) ){
            $file = date("YmdHis_").$files['usuario_imagem']['name'];
            $realFile = $this->imgUserPath.$file;
            $tmp_file = $files['usuario_imagem']['tmp_name'];
            if( move_uploaded_file($tmp_file,$realFile) ){
                $data['usuario_imagem'] = $file;
            }
        }

        if (isset($id) && !empty($id) && $id!='novo') {
            $usuario = Usuario::edit($id,$data);
        }else{
            $session = $this->getSessions();
            $data['empresa_id'] = $session['empresa_id'];
            $usuario = Usuario::store($data);
        }


        if(!isset($usuario->usuario_id)){
            throw new RestException(404, 'Erro ao tentar salvar o funcionário');
            die();
        }

        $usuario = Usuario::getOne($usuario->usuario_id);

        return array("success"=>"true","usuario"=>$usuario,"msg"=>"Perfil salvo com sucesso");
    }


    /**
     * Trazer todos os clientes da agenda
     *
     * @url POST /funcionario/agenda/$id
     */
    public function getClientes(){
        $data = $this->getAll();
        $clientes = Agenda::getClientes(['usuario_id'=>$data['usuario_id']]);

        return array("success"=>"true","clientes"=>$clientes,"msg"=>"Clientes");
    }
}