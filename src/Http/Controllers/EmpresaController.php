<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 08/10/18
 * Time: 18:14
 */

namespace PontoCo\Http\Controllers;

use PontoCo\Helpers\Datatable;
use Jacwright\RestServer\RestException;
use PontoCo\Models\Empresa;

class EmpresaController extends BaseController{
    /**
     * Mostra a tela de login
     *
     * @url GET /empresa
     */
    public function index(){
        $empresa = Empresa::getOne("1");

        $this->returnView('empresa.crud', ['empresa' => $empresa]);
    }
    /**
     * Edição/Criação de Empresa
     *
     * @url POST /empresa/$id
     */
    public function edit($id=null){
        $data = $this->getAll();


        foreach($data['empresa'] AS $k=>$v){
            $data[$k] = $v;
        }

        $empresa = Empresa::edit($id,$data);


        if(!isset($empresa->empresa_id)){
            throw new RestException(404, 'Erro ao tentar salvar a empresa');
            die();
        }

        return array("success"=>"true","empresa"=>$empresa);
    }
}