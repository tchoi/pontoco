<?php
/**
 * Created by PhpStorm.
 * User: Tchoi
 * Date: 26/10/2018
 * Time: 21:29
 */

namespace PontoCo\Http\Controllers;


use Jacwright\RestServer\RestException;
use PontoCo\Helpers\Datatable;
use PontoCo\Helpers\Formatters;
use PontoCo\Models\Contato;

class ContatoController extends BaseController
{
    /**
     * Mostra a tela
     *
     * @url GET /contatos
     */
    public function index(){
        $this->returnView('registro.lista', ['name' => 'hello world']);
    }

    /**
     * Listagem de agendas
     *
     * @url GET /contatos/lista
     * @url POST /contatos/lista
     */
    public function lista(){
        $data = $this->getAll();
        $lista = Contato::getList($data);
        if(isset($data['mobile'])){
            return $this->returnToMobile($lista);
        }else{
            return Datatable::make($data,$lista);
        }
    }

    /**
     * Edição/Criação de Registro
     *
     * @url POST /contato/$id
     * @url POST /contato/novo
     */
    public function edit($id=null){
        $data = $this->getAll();
        $data['contato_dh'] = isset($data['contato_dh']) && !empty($data['contato_dh']) ? Formatters::dateBR2DB($data['contato_dh']) : date("Y-m-d H:i:s");

        if (isset($id) && !empty($id) && $id!='novo') {
            $registro = Contato::edit($id,$data);
        }else{
            $registro = Contato::store($data);
        }
        if(!isset($registro->contato_id)){
            throw new RestException(403, 'Erro ao tentar salvar o contato');
            die();
        }
        return array("msg"=>"Contato salvo com sucesso","success"=>true,"contato"=>$registro);
    }
}