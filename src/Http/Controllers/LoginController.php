<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 14/09/18
 * Time: 10:13
 */

namespace PontoCo\Http\Controllers;


use PontoCo\Models\Usuario;
use Jacwright\RestServer\RestException;

class LoginController extends BaseController
{
    /**
     * Mostra a tela de login
     *
     * @url GET /login
     */
    public function index(){
        $this->returnView('login', ['name' => 'hello world']);
    }
    /**
     * Mostra a tela de login
     *
     * @url POST /login
     */
    public function login(){
        $data = $this->getAll();
        $usuario = Usuario::join("empresa","empresa.empresa_id","=","usuario.empresa_id")
            ->where("usuario_email",$data['email'])
            ->where("usuario_senha",$data['senha'])
            ->select("usuario.*","empresa.empresa_razao_social")
            ->whereIn("usuario.nivel_usuario_id",['1','2'])
            ->first();

        if(!isset($usuario->usuario_id)){
            throw new RestException(404, 'Usuário e/ou senha inválidos');
            die();
        }
        if(!isset($data['mobile'])){
            $this->registerSession($usuario->toArray());

            redirect();
        }else{
            $usuario->dispositivo_identificador = $data['dispositivo_identificador'];
            $usuario->dispositivo_app_version = $data['dispositivo_app_version'];
            $usuario->dispositivo_token = $data['dispositivo_token'];
            $usuario->save();
            return ['usuario'=>$usuario,'msg'=>"Funcionario logado com sucesso",'success'=>true];
        }

    }

    /**
     * Mostra a tela de login
     *
     * @url GET /login-direct
     */
    public function loginDirect(){
        $data = $this->getAll();

        $data = json_decode(base64_decode($data['loginDirect']),true);



        $usuario = Usuario::join("empresa","empresa.empresa_id","=","usuario.empresa_id")
            ->where("usuario_email",$data['email'])
            ->where("usuario_senha",$data['senha'])
            ->select("usuario.*","empresa.empresa_razao_social")
            ->whereIn("usuario.nivel_usuario_id",['1','2'])
            ->first();

        if(!isset($usuario->usuario_id)){
            throw new RestException(404, 'Usuário e/ou senha inválidos');
            die();
        }
        if(!isset($data['mobile'])){
            $this->registerSession($usuario->toArray());
            $url = isset($data['url']) ? $data['url'] : "";
            redirect($url);
        }else{
            $usuario->dispositivo_identificador = $data['dispositivo_identificador'];
            $usuario->dispositivo_app_version = $data['dispositivo_app_version'];
            $usuario->dispositivo_token = $data['dispositivo_token'];
            $usuario->save();
            return ['usuario'=>$usuario,'msg'=>"Funcionario logado com sucesso",'success'=>true];
        }

    }
    /**
     * Mostra a tela de login
     *
     * @url GET /logoff
     * @url POST /logoff
     */
    public function logoff(){
        $this->unRegisterSessionAll();
        redirect("login");
    }
}