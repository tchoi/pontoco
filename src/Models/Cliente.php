<?php

namespace PontoCo\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'cliente';
    protected $primaryKey = 'cliente_id';
    public $timestamps = false;
    protected $fillable = [
        'empresa_id',
        'cliente_razao_social',
        'cliente_nome_fantasia',
        'cliente_tipo_pessoa',
        'cliente_documento',
        'cliente_ie',
        'cliente_logo',
        'cliente_responsavel',
        'cliente_responsavel_email',
        'cliente_endereco_cep',
        'cliente_endereco_logradouro',
        'cliente_endereco_numero',
        'cliente_endereco_complemento',
        'cliente_endereco_bairro',
        'cliente_endereco_cidade',
        'cliente_endereco_uf',
        'cliente_endereco_latitude',
        'cliente_endereco_longitude',
    ];

    protected $appends = [
        'action'
    ];

    public function getActionAttribute(){
        return $this->cliente_id;
    }

    public static function getList($data=null){
        $lista = self::orderBy("cliente_nome_fantasia","ASC");
        return $lista;
    }
    public static function edit($id,$data){
        $reg = self::find($id);
        $reg->fill($data);
        return $reg->save() ? $reg : false;

    }
    public static function store($data){
        $reg = new Cliente();
        $reg->fill($data);
        return $reg->save() ? $reg : false;
    }
    public static function getOne($id){
        return self::where('cliente_id',$id)->first();
    }

}
