<?php

namespace PontoCo\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioNivel extends Model
{
    protected $table = 'nivel_usuario';
    public $timestamps = false;
    protected $primaryKey = 'nivel_usuario_id';
}
