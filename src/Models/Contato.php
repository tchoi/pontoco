<?php
/**
 * Created by PhpStorm.
 * User: Tchoi
 * Date: 26/10/2018
 * Time: 21:25
 */

namespace PontoCo\Models;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $table = 'contato';
    protected $primaryKey = 'contato_id';
    public $timestamps = false;

    protected $fillable = [
            'contato_id',
            'usuario_id',
            'contato_titulo',
            'contato_descricao',
            'contato_dh',
            'contato_status'
    ];

    protected $appends = [
            'action',
    ];
    public function getActionAttribute(){
        return $this->contato_id;
    }
    public static function getList($data=null){
        $lista = self::join('usuario','usuario.usuario_id','=','contato.usuario_id')
                ->select("contato.*",'usuario.usuario_nome');

        if( isset($data['usuario_id']) && !empty($data['usuario_id']) ){
            $lista->where('contato.usuario_id','=',$data['usuario_id']);
        }
        if( isset($data['length']) && $data['length'] >= 0){
            $lista->skip($data['start']);
            $lista->take($data['length']);
        }

        $lista->orderBy("contato_dh","desc");
        return $lista;
    }
    public static function edit($id,$data){
        $reg = self::find($id);
        $reg->fill($data);
        return $reg->save() ? $reg : false;

    }
    public static function store($data){
        $reg = new Contato();
        $reg->fill($data);
        return $reg->save() ? $reg : false;
    }
    public static function getOne($id){
        return self::where('contato_id',$id)->first();
    }

}