<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 08/10/18
 * Time: 08:31
 */

namespace PontoCo\Models;

use Illuminate\Database\Eloquent\Model;
class Empresa extends Model
{
    protected $table = 'empresa';
    protected $primaryKey = 'empresa_id';
    public $timestamps = false;

    protected $fillable = [
        'empresa_tipo_pessoa',
        'empresa_razao_social',
        'empresa_nome_fantasia',
        'empresa_documento',
        'empresa_cep',
        'empresa_logradouro',
        'empresa_numero',
        'empresa_complemento',
        'empresa_bairro',
        'empresa_cidade',
        'empresa_uf',
        'empresa_latitude',
        'empresa_longitude',
    ];

    protected $appends = [
        'action'
    ];

    public function getActionAttribute(){
        return $this->empresa_id;
    }
    public static function getList($data=null){
        $lista = self::orderBy("empresa_razao_social","ASC");
        return $lista;

    }
    public static function edit($id,$data){
        $reg = self::find($id);
        $reg->fill($data);
        return $reg->save() ? $reg : false;

    }
    public static function store($data){
        $reg = new Empresa();
        $reg->fill($data);
        return $reg->save() ? $reg : false;
    }
    public static function getOne($id){
        return self::where('empresa_id',$id)->first();
    }
}