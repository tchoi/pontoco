<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 14/09/18
 * Time: 09:48
 */

namespace PontoCo\Models;

use Illuminate\Database\Eloquent\Model;
class Usuario extends Model
{
    protected $table = 'usuario';
    protected $primaryKey = 'usuario_id';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'empresa_id',
        'nivel_usuario_id',
        'usuario_nome',
        'usuario_email',
        'usuario_senha',
        'usuario_rg',
        'usuario_cpf',
        'usuario_imagem',
        'usuario_dt_nascimento',
        'usuario_residencia_logradouro',
        'usuario_residencia_numero',
        'usuario_residencia_bairro',
        'usuario_residencia_cidade',
        'usuario_residencia_uf',
        'usuario_residencia_cep',
        'usuario_residencia_latitude',
        'usuario_residencia_longitude'
    ];


    protected $appends = array(
        'usuario_nivel_label',
        'action'
    );

    public function getUsuarioNivelLabelAttribute(){
        return UsuarioNivel::where("nivel_usuario_id",$this->nivel_usuario_id)->first()->nivel_usuario_titulo;
    }

    public function getActionAttribute(){
        return $this->usuario_id;
    }
    public static function getList($data=null){
        $lista = self::orderBy("usuario_nome","ASC");
        return $lista;

    }
    public static function edit($id,$data){
        $reg = self::find($id);
        $reg->fill($data);
        return $reg->save() ? $reg : false;

    }
    public static function store($data){
        $reg = new Usuario();
        $reg->fill($data);
        return $reg->save() ? $reg : false;
    }
    public static function getOne($id){
        return self::join("empresa","empresa.empresa_id","=","usuario.empresa_id")
                ->select("usuario.*","empresa.empresa_razao_social")
                ->where('usuario_id','=',$id)->first();
    }



}