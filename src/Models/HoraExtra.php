<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 06/11/18
 * Time: 08:27
 */

namespace PontoCo\Models;


use Illuminate\Database\Eloquent\Model;

class HoraExtra extends Model
{
    protected $table = 'hora_extra';
    protected $primaryKey = 'hora_extra_id';
    public $timestamps = false;

    protected $fillable = [
        'usuario_id',
        'cliente_id',
        'usuario_id',
        'usuario_id_aprovador',
        'hora_extra_motivo',
        'hora_extra_periodo_ini',
        'hora_extra_periodo_fim',
        'hora_extra_motivo_status',
        'hora_extra_status',
        'hora_extra_dh',
    ];

    protected $appends = [
        'action',
    ];
    public function getActionAttribute(){
        return $this->hora_extra_id;
    }

    public static function getList($data=null){
        $lista = self::join('cliente','cliente.cliente_id','=','hora_extra.cliente_id')
            ->join('usuario','usuario.usuario_id','=','hora_extra.usuario_id')
            ->leftJoin('usuario as usuario_aprovador','usuario_aprovador.usuario_id','=','hora_extra.usuario_id')
            ->select("hora_extra.*",'cliente.cliente_razao_social','usuario.usuario_nome','usuario_aprovador.usuario_nome as aprovador');

        if( isset($data['usuario_id']) && !empty($data['usuario_id']) ){
            $lista->where('hora_extra.usuario_id','=',$data['usuario_id']);
        }
        $lista->orderBy("hora_extra.hora_extra_dh","ASC");
        return $lista;
    }
    public static function edit($id,$data){
        $reg = self::find($id);
        $reg->fill($data);
        return $reg->save() ? $reg : false;

    }
    public static function store($data){
        $reg = new HoraExtra();
        $reg->fill($data);
        return $reg->save() ? $reg : false;
    }
    public static function getOne($id){
        return self::join('cliente','cliente.cliente_id','=','hora_extra.cliente_id')
            ->join('usuario','usuario.usuario_id','=','hora_extra.usuario_id')
            ->leftJoin('usuario as usuario_aprovador','usuario_aprovador.usuario_id','=','hora_extra.usuario_id')
            ->select("hora_extra.*",'cliente.cliente_razao_social','usuario.usuario_nome','usuario_aprovador.usuario_nome as aprovador')
            ->where('hora_extra.hora_extra_id',$id)->first();
    }

    public static function verificaHoraExtra($data){
        $reg = self::where('usuario_id',$data['usuario_id'])
            ->where('cliente_id',$data['cliente_id'])
            ->whereRaw("'{$data['dh']}' BETWEEN hora_extra_periodo_ini AND hora_extra_periodo_fim")
            ->orderBy('hora_extra_dh','DESC');
        return $reg->first();
    }

}