<?php
/**
 * Created by PhpStorm.
 * User: Tchoi
 * Date: 28/10/2018
 * Time: 21:50
 */

namespace PontoCo\Models;

use Illuminate\Database\Eloquent\Model;

class Justificativa extends Model
{
    protected $table = 'justificativa';
    protected $primaryKey = 'justificativa_id';
    public $timestamps = false;

    protected $fillable = [
            'usuario_id',
            'justificativa_dt',
            'justificativa_titulo',
            'justificativa_motivo',
            'justificativa_arquivo',
            'justificativa_status',
            'justificativa_duracao_ini',
            'justificativa_duracao_fim'
    ];
    protected $appends = [
            'action',
    ];
    public function getActionAttribute(){
        return $this->justificativa_id;
    }
    public static function getList($data=null){
        $lista = self::join('usuario','usuario.usuario_id','=','justificativa.usuario_id')
                ->select("justificativa.*",'usuario.usuario_nome');
        if( isset($data['usuario_id']) && !empty($data['usuario_id']) ){
            $lista->where('justificativa.usuario_id','=',$data['usuario_id']);
        }
        $lista->orderBy("justificativa_dt","desc");
        return $lista;
    }
    public static function edit($id,$data){
        $reg = self::find($id);
        $reg->fill($data);
        return $reg->save() ? $reg : false;

    }
    public static function store($data){
        $reg = new Justificativa();
        $reg->fill($data);
        return $reg->save() ? $reg : false;
    }
    public static function getOne($id){
        return self::where('justificativa_id',$id)->first();
    }

}