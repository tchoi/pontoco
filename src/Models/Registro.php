<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 08/10/18
 * Time: 09:02
 */

namespace PontoCo\Models;

use Illuminate\Database\Eloquent\Model;
class Registro extends Model
{
    protected $table = 'registro';
    protected $primaryKey = 'registro_id';
    public $timestamps = false;

    protected $fillable = [
        'usuario_id',
        'cliente_id',
        'registro_dh',
        'registro_latitude',
        'registro_longitude',
        'registro_tipo',
        'registro_motivo'
    ];

    protected $appends = [
        'action'
    ];
    public function getActionAttribute(){
        return $this->registro_id;
    }
    public static function getList($data=null){
        $lista = self::join("usuario",'usuario.usuario_id','=','registro.usuario_id')
            ->join("cliente",'cliente.cliente_id','=','registro.cliente_id')
            ->select('registro.*','cliente.cliente_razao_social','usuario.usuario_nome');

        if( isset($data['usuario_id']) && !empty($data['usuario_id']) ){
            $lista->where('registro.usuario_id',$data['usuario_id']);
        }
        $lista->orderBy("registro.registro_dh","DESC");

        if( isset($data['length']) && $data['length'] >= 0){
            $lista->skip($data['start']);
            $lista->take($data['length']);
        }
        return $lista;
    }
    public static function edit($id,$data){
        $reg = self::find($id);
        $reg->fill($data);
        return $reg->save() ? $reg : false;

    }
    public static function store($data){
        $reg = new Registro();
        $reg->fill($data);
        return $reg->save() ? $reg : false;
    }
    public static function getOne($id){
        return self::where('registro_id',$id)->first();
    }
    public static function getRegistro($id){
        return self::join("usuario",'usuario.usuario_id','=','registro.usuario_id')
                ->join("cliente",'cliente.cliente_id','=','registro.cliente_id')
                ->select(
                        'registro.*',
                        'cliente.cliente_razao_social',
                        'cliente.cliente_endereco_latitude',
                        'cliente.cliente_endereco_longitude',
                        'usuario.usuario_nome')
                ->where('registro.registro_id',$id)->first();;

    }
    public static function verifyUltimoRegistro($data,$tiposPermitidos=array('entrada_01','saida_01','entrada_02','saida_02')){
        $reg = self::where('usuario_id',$data['usuario_id']);
        $reg->where('cliente_id',$data['cliente_id']);
        $reg->where('registro_dh','>=',$data['registro_dh_ini']);
        $reg->where('registro_dh','<=',$data['registro_dh_fim']);
        if(count($tiposPermitidos) > 0){
            $reg->whereIn('registro_tipo',$tiposPermitidos);
        }
        $reg->orderBy('registro_dh','DESC');
        return $reg->first();
    }
}