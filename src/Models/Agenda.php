<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 08/10/18
 * Time: 08:38
 */

namespace PontoCo\Models;

use Illuminate\Database\Eloquent\Model;
use PontoCo\Helpers\Formatters;

class Agenda extends Model
{
    protected $table = 'agenda';
    protected $primaryKey = 'agenda_id';
    public $timestamps = false;

    protected $fillable = [
        'usuario_id',
        'cliente_id',
        'agenda_hora_entrada_01',
        'agenda_hora_entrada_02',
        'agenda_hora_saida_01',
        'agenda_hora_saida_02',
        'agenda_dt_ini',
        'agenda_dt_fim',
        'agenda_dia_semana'
    ];

    protected $appends = [
        'action',
        'agenda_dia_semana_label'
    ];
    public function getActionAttribute(){
        return $this->agenda_id;
    }
    public function getAgendaDiaSemanaLabelAttribute(){
        $aDiaSemana =  isset($this->agenda_dia_semana) ? explode(",",$this->agenda_dia_semana) : array();
        $aDiaSemana =  count($aDiaSemana) > 0 ? Formatters::diaSemanaByArrayNumber($aDiaSemana,true) : array();
        return count($aDiaSemana) > 0 ? implode(",",$aDiaSemana) : "";
    }

    public static function getList($data=null){
        $lista = self::join('cliente','cliente.cliente_id','=','agenda.cliente_id')
                ->join('usuario','usuario.usuario_id','=','agenda.usuario_id')
                ->select("agenda.*",'cliente.cliente_razao_social','usuario.usuario_nome');

        if( isset($data['usuario_id']) && !empty($data['usuario_id']) ){
            $lista->where('agenda.usuario_id','=',$data['usuario_id']);
        }
        $lista->orderBy("agenda_dt_ini","ASC");
        return $lista;
    }
    public static function edit($id,$data){
        $reg = self::find($id);
        $reg->fill($data);
        return $reg->save() ? $reg : false;

    }
    public static function store($data){
        $reg = new Agenda();
        $reg->fill($data);
        return $reg->save() ? $reg : false;
    }
    public static function getOne($id){
        return self::where('agenda_id',$id)->first();
    }

    public static function getClienteByLatLng($data){
        $lista = self::join('cliente','cliente.cliente_id','=','agenda.cliente_id')
            ->select('agenda.*','cliente.cliente_razao_social');
        $lista->where('agenda.usuario_id','=',$data['usuario_id']);
        $lista->whereRaw(" (DISTANCEGPS(cliente.cliente_endereco_latitude,cliente.cliente_endereco_longitude,{$data['registro_latitude']},{$data['registro_longitude']},'KM') * 1000) <= 400 ");
        return $lista->first();
    }

    public static function getClienteByHorario($data){
        $dt = date('Y-m-d',strtotime($data['dh']));

        $lista = self::join('cliente','cliente.cliente_id','=','agenda.cliente_id')
            ->select(
                    'agenda.*',
                    'cliente.cliente_razao_social',
                    'cliente.cliente_endereco_longitude',
                    'cliente.cliente_endereco_latitude',
                    'cliente.cliente_endereco_cep');

        $lista->where('agenda.usuario_id','=',$data['usuario_id']);
        $lista->whereRaw("agenda.agenda_dia_semana LIKE '%{$data['agenda_dia_semana']}%'");

        $lista->whereRaw("'{$dt}' >= agenda.agenda_dt_ini");
        $lista->whereRaw("(agenda.agenda_dt_fim IS NULL OR '{$dt}' <= agenda.agenda_dt_fim)");


        $lista->whereRaw("(
	        '{$data['dh']}' between concat('{$dt}',' ',agenda_hora_entrada_01) AND concat('{$dt}',' ',agenda_hora_saida_01)
            OR '{$data['dh']}' between concat('{$dt}',' ',agenda_hora_entrada_02) AND concat('{$dt}',' ',agenda_hora_saida_02))");
        return $lista->first();
    }

    public static function getClienteByHorarioLista($data){
        $dt = date('Y-m-d',strtotime($data['dh']));

        $lista = self::join('cliente','cliente.cliente_id','=','agenda.cliente_id')
                ->select(
                        'agenda.*',
                        'cliente.cliente_razao_social',
                        'cliente.cliente_endereco_longitude',
                        'cliente.cliente_endereco_latitude',
                        'cliente.cliente_endereco_cep');

        $lista->where('agenda.usuario_id','=',$data['usuario_id']);
        $lista->whereRaw("(agenda.agenda_dia_semana LIKE '%{$data['agenda_dia_semana']}%' OR agenda.agenda_dia_semana LIKE '%7%')");

        $lista->whereRaw("'{$dt}' >= agenda.agenda_dt_ini");
        $lista->whereRaw("(agenda.agenda_dt_fim IS NULL OR '{$dt}' <= agenda.agenda_dt_fim)");

        $lista->whereRaw("(
	        '{$data['dh']}' between concat('{$dt}',' ',agenda_hora_entrada_01) AND concat('{$dt}',' ',agenda_hora_saida_01)
            OR '{$data['dh']}' between concat('{$dt}',' ',agenda_hora_entrada_02) AND concat('{$dt}',' ',agenda_hora_saida_02))");

        return $lista->get();
    }

    public static function getClientes($data){
        $lista = self::join('cliente','cliente.cliente_id','=','agenda.cliente_id')
                ->join('usuario','usuario.usuario_id','=','agenda.usuario_id')
                ->select("cliente.*");

        if( isset($data['usuario_id']) && !empty($data['usuario_id']) ){
            $lista->where('agenda.usuario_id','=',$data['usuario_id']);
        }
        $lista->orderBy("cliente.cliente_razao_social","ASC");
        return $lista->get();
    }
}