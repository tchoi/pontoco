<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 14/09/18
 * Time: 10:21
 */

namespace PontoCo\Helpers;


class Formatters
{
    public static function diasemana($data) {
        $data = explode("-", $data);
        $ano = $data[0];
        $mes = $data[1];
        $dia = $data[2];
        $diasemana = date("w", mktime(0, 0, 0, $mes, $dia, $ano));
        switch ($diasemana) {
            case"0": $diasemana = "Domingo";
                break;
            case"1": $diasemana = "Segunda-Feira";
                break;
            case"2": $diasemana = "Terça-Feira";
                break;
            case"3": $diasemana = "Quarta-Feira";
                break;
            case"4": $diasemana = "Quinta-Feira";
                break;
            case"5": $diasemana = "Sexta-Feira";
                break;
            case"6": $diasemana = "Sábado";
                break;
        }

        return $diasemana;
    }
    public static function diaSemanaByNumber($diasemana,$abbr=true) {

        switch ($diasemana) {
            case"0": $diasemana = $abbr ? "Dom":"Domingo";
                break;
            case"1": $diasemana = $abbr ? "Seg":"Segunda-Feira";
                break;
            case"2": $diasemana = $abbr ? "Ter":"Terça-Feira";
                break;
            case"3": $diasemana = $abbr ? "Qua":"Quarta-Feira";
                break;
            case"4": $diasemana = $abbr ? "Qui":"Quinta-Feira";
                break;
            case"5": $diasemana = $abbr ? "Sex":"Sexta-Feira";
                break;
            case"6": $diasemana = $abbr ? "Sáb":"Sábado";
                break;
            case"7": $diasemana = $abbr ? "Fer":"Feriado";
                break;
        }

        return $diasemana;
    }
    public static function diaSemanaByArrayNumber($aDiaSemana,$abbr=true){
        $aRet = [];
        foreach($aDiaSemana AS $d){
            $aRet[] = valueText(self::diaSemanaByNumber($d,$abbr),"");
        }
        return $aRet;
    }

    public static function formatFields($string, $type = "") {
        $output = trim(preg_replace("[' '-./ t]", '', str_replace(array('(', ')'), '', $string)));
        switch (strtolower($type)) {
            case 'cpf':
                return preg_replace(
                    "/([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})/i", "$1.$2.$3-$4", $output
                );
                break;
            case 'cnpj':
                return preg_replace(
                    "/([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})/i", "$1.$2.$3/$4-$5", $output
                );
                break;
            case 'cep':
                return preg_replace(
                    "/([0-9]{5})([0-9]{3})/i", "$1-$2", $output
                );
                break;
            case 'rg':
                return preg_replace(
                    "/([0-9]{2})([0-9]{3})([0-9]{3})([0-9a-zA-Z]{1})/i", "$1.$2.$3-$4", $output
                );
                break;
            case 'tel':
                if (strlen($output) == 10) {
                    return preg_replace(
                        "/([0-9]{2})([0-9]{4})([0-9]{4})/i", "($1)$2-$3", $output
                    );
                } else if (strlen($output) == 11) {
                    return preg_replace(
                        "/([0-9]{2})([0-9]{5})([0-9]{4})/i", "($1)$2-$3", $output
                    );
                }
                break;
        }
        return $string;
    }

    public static function dateDB2BR($date, $separete = "/") {
        return preg_replace(
            "/([0-9]{4})-([0-9]{2})-([0-9]{2})/i", "$3{$separete}$2{$separete}$1", $date
        );
    }

    public static function dateBR2DB($date, $separete = "-") {
        return preg_replace(
            "/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/i", "$3{$separete}$2{$separete}$1", $date
        );
    }

    public static function dateDB2BRTime($date, $separete = "/") {
        return preg_replace(
            "/([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})/i", "$3{$separete}$2{$separete}$1 $4:$5:$6", $date
        );
    }

    public static function dateBR2DBTime($date, $separete = "/") {
        return preg_replace(
            "/([0-9]{2})\/([0-9]{2})\/([0-9]{4}) ([0-9]{2}):([0-9]{2}):([0-9]{2})/i", "$3{$separete}$2{$separete}$1 $4:$5:$6", $date
        );
    }


    public static function limpaDoc($doc, $tipo = "") {
        return str_replace(array(".", "/", "-"), "", $doc);
    }

    public static function limpaMoeda($moeda, $tipo = "") {
        $moeda = trim(str_replace(array("R$",'r$','$'),"",$moeda));
        $moeda = str_replace(".", "", $moeda);
        $moeda = str_replace(",", ".", $moeda);
        return $moeda;
    }

    public static function limpaTel($tel, $tipo = "") {
        return str_replace(array("(", ")", " ", "-"), "", $tel);
    }

    public static function limpaCep($cep, $tipo = "") {
        return str_replace(array("(", ")", " ", "-"), "", $cep);
    }

    public static function formataMoeda($moeda) {
        return number_format($moeda, 2, ",", ".");
    }

    public static function getMonthName($nMonth) {
        switch ($nMonth) {
            case 1:
                return 'JAN';
                break;
            case 2:
                return 'FEV';
                break;
            case 3:
                return 'MAR';
                break;
            case 4:
                return 'ABR';
                break;
            case 5:
                return 'MAI';
                break;
            case 6:
                return 'JUN';
                break;
            case 7:
                return 'JUL';
                break;
            case 8:
                return 'AGO';
                break;
            case 9:
                return 'SET';
                break;
            case 10:
                return 'OUT';
                break;
            case 11:
                return 'NOV';
                break;
            case 12:
                return 'DEZ';
                break;
        }
    }
}