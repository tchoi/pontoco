<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 18/09/18
 * Time: 09:07
 */

namespace PontoCo\Helpers;


class Datatable
{
    public static $request;
    public static $query;
    public static $records_total;
    public static $records_filtered;

    static function make ($request, $model) {
        self::$request = $request;
        self::$query = $model;
        self::setRecordsTotal();
        self::filter();
        self::setRecordsFiltered();
        self::orderLimit();
        return self::render();
    }

    // set total record count
    static function setRecordsTotal () {
        self::$records_total = self::$query->count();
    }

    // filter by search query
    static function filter () {
        if (!empty(self::$request['search']['value'])) {
            foreach (self::$request['columns'] as $column) {
                if ($column['searchable'] == 'true') {
                    self::$query->orWhere($column['data'], 'like', '%'.self::$request['search']['value'].'%');
                }
            }
        }
    }

    // set filtered record count
    static function setRecordsFiltered () {
        self::$records_filtered = self::$query->count();
    }

    // apply order by & limit
    static function orderLimit () {
        self::$query->orderBy(self::$request['columns'][self::$request['order'][0]['column']]['data'], self::$request['order'][0]['dir']);
        if(self::$request['length'] >= 0){
            self::$query->skip(self::$request['start'])->take(self::$request['length']);
        }
    }

    // render json output
    static function renderJson () {
        $array = [];
        $array['draw'] = self::$request['draw'];
        $array['recordsTotal'] = self::$records_total;
        $array['recordsFiltered'] = self::$records_filtered;
        $array['data'] = [];
        $results = self::$query->get();


        foreach ($results as $result) {
            $array['data'][] = $result->toArray();
        }

        echo json_encode($array);
    }
    static function render() {
        $array = [];
        $array['draw'] = self::$request['draw'];
        $array['recordsTotal'] = self::$records_total;
        $array['recordsFiltered'] = self::$records_filtered;
        $array['data'] = [];
        $results = self::$query->get();
        foreach ($results as $result) {
            $array['data'][] = $result->toArray();
        }

        return $array;
    }
}