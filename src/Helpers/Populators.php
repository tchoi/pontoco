<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 14/09/18
 * Time: 10:26
 */

namespace PontoCo\Helpers;


class Populators
{
    protected static $uf = array(
        "AC" => "Acre"
        , "AL" => "Alagoas"
        , "AM" => "Amazonas"
        , "AP" => "Amapa"
        , "BA" => "Bahia"
        , "CE" => "Ceará"
        , "DF" => "Distrito Federal"
        , "ES" => "Espirito Santo"
        , "GO" => "Goiás"
        , "MA" => "Maranhão"
        , "MG" => "Minas Gerais"
        , "MS" => "Mato Grosso do Sul"
        , "MT" => "Mato Grosso"
        , "PA" => "Pará"
        , "PB" => "Paraíba"
        , "PE" => "Pernambuco"
        , "PI" => "Piauí"
        , "PR" => "Parana"
        , "RJ" => "Rio de Janeiro"
        , "RN" => "Rio Grande do Norte"
        , "RO" => "Rondônia"
        , "RR" => "Roraima"
        , "RS" => "Rio Grande do Sul"
        , "SC" => "Santa Catarina"
        , "SE" => "Sergipe"
        , "SP" => "São Paulo"
        , "TO" => "Tocantins"
    );
    protected static  $meses = array(
        "01" => "Janeiro"
        , "02" => "Fevereiro"
        , "03" => "Março"
        , "04" => "Abril"
        , "05" => "Maio"
        , "06" => "Junho"
        , "07" => "Julho"
        , "08" => "Agosto"
        , "09" => "Setembro"
        , "10" => "Outubro"
        , "11" => "Novembro"
        , "12" => "Dezembro"
    );

    protected static $dia_semana = array(
        0=>'Domingo',
        1=>'Segunda-Feira',
        2=>'Terça-Feira',
        3=>'Quarta-Feira',
        4=>'Quinta-Feira',
        5=>'Sexta-Feira',
        6=>'Sabado',
        7=>'Feriado',
    );

    /**
     * @return array
     */
    public static function getUf()
    {
        return self::$uf;
    }

    /**
     * @return array
     */
    public static function getMeses()
    {
        return self::$meses;
    }

    /**
     * @return array
     */
    public static function getDiaSemana()
    {
        return self::$dia_semana;
    }


}