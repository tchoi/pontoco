<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 14/09/18
 * Time: 10:23
 */

namespace PontoCo\Helpers;


class Sanetizers
{
    public static function antiInjection($input) {
        if (is_array($input)) {
            foreach ($input as $key => $val) {
                $sql = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/", "", $val);
                $sql = trim($sql);
                $sql = (get_magic_quotes_gpc()) ? $sql : addslashes($sql);
                $input[$key] = $sql;
            }
            return $input;
        }
        $sql = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/", "", $input);
        $sql = trim($sql);
        $sql = (get_magic_quotes_gpc()) ? $sql : addslashes($sql);
        return $sql;
    }

    public static function utf8_array_encode($input) {
        $return = array();
        if (is_array($input)) {
            foreach ($input as $key => $val) {
                if (is_array($val)) {
                    $return[$key] = self::utf8_array_encode($val);
                } else {
                    $return[$key] = self::utf8Encode2Decode($val);
                }
            }
        }
        return $return;
    }

    public static function utf8Encode2Decode($string) {
        if (strtoupper(mb_detect_encoding($string . "x", 'UTF-8, ISO-8859-1')) == 'UTF-8') {
            return str_replace(array("\'",'\"'),array("'",'"'),$string);
        } else {
            return utf8_encode(str_replace(array("\'",'\"'),array("'",'"'),$string));
        }
        return $string;
    }
    public static function normaliza($string) {
        $a = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyrc';
        $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyrc';
        $string = self::utf8Encode2Decode($string);
        $string = strtr($string, self::utf8Encode2Decode($a), $b);
        $string = str_replace(' ', '_', $string);
        $string = strtolower($string);
        return         $string = strtr($string, self::utf8Encode2Decode($a), $b);
        utf8Encode2Decode($string);
    }

    public static function toNormaliza($string) {
        $array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç"
        , "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç", 'º', 'ª', '-');
        $array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c"
        , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C", '', '', '');
        $string = str_replace(' ', '_', $string);
        $string = str_replace('/', '.', $string);
        return strtolower(str_replace($array1, $array2, $string));
    }
}