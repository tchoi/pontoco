<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 14/09/18
 * Time: 10:24
 */

namespace PontoCo\Helpers;


class DateHandlers
{
    public static function timeSum($aHora) {
        $hora = 0;
        $minuto = 0;
        $segundo = 0;
        if (is_array($aHora) && count($aHora)) {
            foreach ($aHora as $v) {
                $tem = explode(":", $v);
                $hora += $tem[0];
                $minuto += $tem[1];
                $segundo += (isset($tem[2])) ? $tem[2] : 0;
            }
        }
        $secMin = floor($segundo / 60);
        $segundo = $segundo - ($secMin * 60);

        $minuto += $secMin;
        $horaMin = floor($minuto / 60);
        $hora += $horaMin;
        $minuto = $minuto - ($horaMin * 60);

        $hora = str_pad($hora, 2, '0', STR_PAD_LEFT);
        $minuto = str_pad($minuto, 2, '0', STR_PAD_LEFT);
        $segundo = str_pad($segundo, 2, '0', STR_PAD_LEFT);

        return "{$hora}:{$minuto}:{$segundo}";
    }
}