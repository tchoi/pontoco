<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 14/09/18
 * Time: 13:41
 */

use Jacwright\RestServer\RestServer;

require_once "bootstrap.php";
require_once "funcoes.php";

if (session_id() == '') {
    session_name(\PontoCo\Helpers\Sanetizers::normaliza(app_name())."-2018");
    session_start();
}

$mode = 'debug'; // 'debug' or 'production'
$server = new RestServer($mode);
$server->refreshCache(); // uncomment momentarily to clear the cache if classes change in production mode
$server->addClass(PontoCo\Http\Controllers\HomeController::class);
$server->addClass(PontoCo\Http\Controllers\LoginController::class);
$server->addClass(PontoCo\Http\Controllers\ClienteController::class);
$server->addClass(PontoCo\Http\Controllers\EmpresaController::class);
$server->addClass(PontoCo\Http\Controllers\UsuarioController::class);
$server->addClass(PontoCo\Http\Controllers\AgendaController::class);
$server->addClass(PontoCo\Http\Controllers\RegistroController::class);
$server->addClass(PontoCo\Http\Controllers\ContatoController::class);
$server->addClass(PontoCo\Http\Controllers\JustificativaController::class);
$server->addClass(PontoCo\Http\Controllers\HoraExtraController::class);
$server->handle();