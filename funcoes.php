<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 17/09/18
 * Time: 08:40
 */
function pr($mixed){
    @header("Content-Type: text/html; charset=UTF-8",true);
    echo "<pre>".print_r($mixed,true)."</pre>";
}
function getConfig(){
    $path_config = dirname(__FILE__);
    $DS = DIRECTORY_SEPARATOR;
    $path_config = "{$path_config}{$DS}";
    $path_config = realpath($path_config) . $DS;
    $file = "producao.php";
    if( getenv("MODO") && getenv("MODO")=='dev'){
        $file = "dev.php";
    }else if(getenv("MODO") && getenv("MODO")=='homolog'){
        $file = "homolog.php";
    }

    return include("{$path_config}config{$DS}{$file}");
}
function url($page=null){
    $config = getConfig();
    if(empty($page))
        return $config['url_base'];
    else
        return $config['url_base'].$page;
}

function url_public(){
    $config = getConfig();
    return $config['url_base']."public/";
}

function titulo_site(){
    $config = getConfig();
    return $config['titulo_site'];
}
function assets($file){
    return  url_public().$file;
}
function app_name(){
    $config = getConfig();
    return $config['app_name'];
}
function redirect($url=''){
    header('Location: '.url($url));
}
function iniciais($nome,$minusculas = true){
    preg_match_all('/\s?([A-Z])/',$nome,$matches);
    $ret = implode('',$matches[1]);
    return $minusculas?
        strtolower($ret) :
        $ret;
}

function getEstados(){
    return [
        "AC"=>"Acre",
        "AL"=>"Alagoas",
        "AP"=>"Amapá",
        "AM"=>"Amazonas",
        "BA"=>"Bahia",
        "CE"=>"Ceará",
        "DF"=>"Distrito Federal",
        "ES"=>"Espírito Santo",
        "GO"=>"Goiás",
        "MA"=>"Maranhão",
        "MT"=>"Mato Grosso",
        "MS"=>"Mato Grosso do Sul",
        "MG"=>"Minas Gerais",
        "PA"=>"Pará",
        "PB"=>"Paraíba",
        "PR"=>"Paraná",
        "PE"=>"Pernambuco",
        "PI"=>"Piauí",
        "RJ"=>"Rio de Janeiro",
        "RN"=>"Rio Grande do Norte",
        "RS"=>"Rio Grande do Sul",
        "RO"=>"Rondônia",
        "RR"=>"Roraima",
        "SC"=>"Santa Catarina",
        "SP"=>"São Paulo",
        "SE"=>"Sergipe",
        "TO"=>"Tocantins",
    ];
}

function valueText($valor,$default=""){
    return isset($valor) ? $valor : $default;
}
function selected($valor,$compare){
    return isset($valor) && $compare==$valor ? "selected" : "";
}
function selectedInArray($arr,$valor){
    return in_array($valor,$arr)!==false ? "selected" : "";
}
