<?php
/**
 * Created by PhpStorm.
 * User: tchoi
 * Date: 14/09/18
 * Time: 09:54
 */
require "vendor/autoload.php";

use Illuminate\Database\Capsule\Manager as Capsule;
$capsule = new Capsule;

if( getenv("MODO") && getenv("MODO")=='dev') {
    $capsule->addConnection([
        "driver" => "mysql",
        "host" => "vps4782.publiccloud.com.br",
        "database" => "walter_dot",
        "username" => "root",
        "password" => "destino2002"
    ]);
}else{
    $capsule->addConnection([
        "driver" => "mysql",
        "host" => "127.0.0.1",
        "database" => "walter_dot",
        "username" => "root",
        "password" => "destino2002"
    ]);
}
//Make this Capsule instance available globally.
$capsule->setAsGlobal();
// Setup the Eloquent ORM.
$capsule->bootEloquent();